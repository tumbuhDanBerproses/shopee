<html>

<head>
    <title>Shopee</title>
    <link rel="shortcut icon" href="http://azisalvriyanto.net/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="row m-3 mb-4" onclick="handleRedirect();">
                    <small class="h5 col-sm-12 text-center text-muted font-weight-bold">
                        Made with <img src="http://static.skaip.org/img/emoticons/180x180/f6fcff/heart.gif" alt="♡" height="13px">
                        by Alvriyanto Azis
                    </small>
                </div>

                <div class="row mt-2 mb-2">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button id="button-reset" class="btn btn-md btn-danger">Reset</button>
                            </div>

                            <input type="text" id="input-url" class="form-control" placeholder="https://shopee.co.id/...">

                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <input type="checkbox" id="is-autoclick" class="mr-2" aria-label="Checkbox for auto click">
                                    Is Auto CLick
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 mt-3">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" id="input-csrf-token" class="form-control" placeholder="CSRF Token" value="YyXlTqL8UDtIFSIsmNJyLIQTj9H2qsS8">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" id="input-spc-si" class="form-control" placeholder="SPC_SI" value="mall.XNCwJggYHPmXVHaoF3uCqT3z29bkRcbh">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 mt-4">
                        <table id="shop" class="table table-sm table-centered table-hover text-left">
                            <tbody>
                                <tr>
                                    <th style="width: 112px;">Shop Id</th>
                                    <th>:</th>
                                    <td id="shop-id" class="information">-</td>
                                </tr>
                                <tr>
                                    <th>Item Id</th>
                                    <th>:</th>
                                    <td id="shop-item-id" class="information">-</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <th>:</th>
                                    <td id="shop-name" class="information">-</td>
                                </tr>
                                <tr>
                                    <th>Discount</th>
                                    <th>:</th>
                                    <td id="shop-discount" class="information">-</td>
                                </tr>
                                <tr>
                                    <th>Shop Location</th>
                                    <th>:</th>
                                    <td id="shop-location" class="information">-</td>
                                </tr>
                                <tr>
                                    <th>Rating Star</th>
                                    <th>:</th>
                                    <td id="shop-rating" class="information">-</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-12 mt-4 table-responsive">
                        <table id="models" class="table table-centered table-hover table-borderless">
                            <thead>
                                <tr class="bg-light text-center">
                                    <th>Model Id</th>
                                    <th>Name</th>
                                    <th>Stock</th>
                                    <th>Price</th>
                                    <th>Is Flashsale</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="text-center">No data available</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        const siteApi = "api.php";

        const handleError = (jqXHR, exception) => {
            if (jqXHR.status === 0) {
                information = "Not connect (verify network).";
            } else if (jqXHR.status == 404) {
                information = "Requested page not found.";
            } else if (jqXHR.status == 500) {
                information = "Internal Server Error.";
            } else if (exception === "parsererror") {
                information = "Requested JSON parse failed.";
            } else if (exception === "timeout") {
                information = "Time out error.";
            } else if (exception === "abort") {
                information = "Ajax request aborted.";
            } else {
                information = "Uncaught Error (" + jqXHR.responseText + ").";
            }

            return information;
        }

        const handleRupiahFormat = (angka, prefix) => {
            var nominal = angka;
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return (nominal < 0 ? '- ' : '') + (prefix == undefined ? ("Rp" + rupiah) : (rupiah ? prefix + rupiah : 0));
        }

        const handleTimesTamp = (UNIX_timestamp) => {
            var a = new Date(UNIX_timestamp * 1000);
            var time = a.getFullYear() + '-' + a.getMonth() + '-' + a.getDate() + ' ' + ("0" + a.getHours()).slice(-2) + ':' + ("0" + a.getMinutes()).slice(-2) + ':' + ("0" + a.getSeconds()).slice(-2);
            return time;
        }

        $("#input-url").bind("input", function() {
            const targetUrl = $(this).val();
            if (targetUrl.match(/shopee.co.id/gi)) {
                $.ajax({
                    url: `${siteApi}?action=get-information`,
                    dataType: "json",
                    type: "POST",
                    data: {
                        "url": targetUrl
                    },
                    beforeSend: function() {
                        $("#input-url").prop("disabled", true);
                        $(".information").html("-");
                        $("#models > tbody").html(`<tr><td colspan="5" class="text-center">No data available</td></tr>`);
                    },
                    success: function(response) {
                        $("#input-url").prop("disabled", false);

                        if (response.status === true) {
                            const data = response.data;

                            $("#shop-id").html(data.shopid);
                            $("#shop-item-id").html(data.itemid);
                            $("#shop-name").html(data.name);
                            $("#shop-discount").html(data.discount);
                            $("#shop-location").html(data.shop_location);

                            $("#shop-rating").html(data.item_rating.rating_star.toString().slice(0, 3));

                            $("#models > tbody").html("");
                            data.models.map((model, key) => {
                                if (data.upcoming_flash_sale) {
                                    isFlashsale = data.upcoming_flash_sale.modelids.includes(model.modelid) ? 'Upcoming (' + handleTimesTamp(data.upcoming_flash_sale.start_time) + ')' : 'False';
                                } else {
                                    if (data.flash_sale) {
                                        isFlashsale = 'True';
                                    } else {
                                        console.log('asw')
                                        isFlashsale = 'False';
                                    }
                                }

                                $("#models > tbody").append(`
                                <tr data-id="${model.modelid}">
                                    <td class="text-center">${model.modelid}</td>
                                    <td>${model.name ? model.name : '-'}</td>
                                    <td class="text-left">${model.stock} (with ${model.sold} sold)</td>
                                    <td class="text-left">${handleRupiahFormat(model.price.toString().slice(0, -5))} of ${handleRupiahFormat(model.price_before_discount.toString().slice(0, -5))}</td>
                                    <td class="text-center">${isFlashsale}</td>
                                    <td class="text-center">
                                        <button type="button" class="button-add-to-cart btn btn-sm btn-success">Add to Cart</button>
                                        <button type="button" class="button-checkout btn btn-sm btn-warning">Checkout</button>
                                    </td>
                                </tr>
                                `)
                            });
                        } else {
                            swal({
                                title: "Try Again",
                                text: response.message,
                                icon: "error",
                                button: "Tutup"
                            });
                        }
                    },
                    error: function(jqXHR, exception) {
                        const information = handleError(jqXHR, exception);

                        swal({
                            title: "Try Again",
                            text: information,
                            icon: "error",
                            button: "Tutup"
                        });

                        $("#input-url").prop("disabled", false);
                    }
                });
            } else {
                $("#input-url").prop("disabled", false);
                $(".information").html("-");
                $("#models > tbody").html(`<tr><td colspan="5" class="text-center">No data available</td></tr>`);
            }
        });

        $("#button-reset").on("click", () => {
            $("#input-url").prop("disabled", false);
            $("#input-url").val("");
            $(".information").html("-");
            $("#models > tbody").html(`<tr><td colspan="5" class="text-center">No data available</td></tr>`);
        });

        $(document).on("click", ".button-add-to-cart", function() {
            const shopId = $("#shop-id").html();
            const itemId = $("#shop-item-id").html();
            const modelId = $(this).parent().parent().data("id");
            const csrfToken = $("#input-csrf-token").val();
            const spcSI = $("#input-spc-si").val();

            $.ajax({
                url: `${siteApi}?action=add-to-cart`,
                dataType: "json",
                type: "POST",
                data: {
                    shop: shopId,
                    item: itemId,
                    model: modelId,
                    csrf_token: csrfToken,
                    spc_si: spcSI
                },
                beforeSend: function() {
                    $("#input-url").prop("disabled", true);
                    $(".button-add-to-cart").prop("disabled", true);
                },
                success: function(response) {
                    $("#input-url").prop("disabled", false);
                    $(".button-add-to-cart").prop("disabled", false);

                    if (response.status === true) {
                        window.open(`https://shopee.co.id/cart/?shopId=${shopId}&itemIds=${itemId}`, "_blank");
                    } else {
                        if ($("#is-autoclick").is(":checked")) {
                            $(this).click();
                        } else {
                            swal({
                                title: "Try Again",
                                text: response.message,
                                icon: "error",
                                button: "Tutup"
                            });
                        };
                    }
                },
                error: function(jqXHR, exception) {
                    const information = handleError(jqXHR, exception);

                    swal({
                        title: "Try Again",
                        text: information,
                        icon: "error",
                        button: "Tutup"
                    });

                    $("#input-url").prop("disabled", false);
                    $(".button-add-to-cart").prop("disabled", false);
                }
            });
        });

        $(document).on("click", ".button-checkout", function() {
            const shopId = $("#shop-id").html();
            const itemId = $("#shop-item-id").html();
            const modelId = $(this).parent().parent().data("id");
            const csrfToken = $("#input-csrf-token").val();
            const spcSI = $("#input-spc-si").val();
            const gclAW = $("#input-gcl-aw").val();

            $.ajax({
                url: `${siteApi}?action=checkout`,
                dataType: "json",
                type: "POST",
                data: {
                    shop: shopId,
                    item: itemId,
                    model: modelId,
                    csrf_token: csrfToken,
                    spc_si: spcSI
                },
                beforeSend: function() {
                    $("#input-url").prop("disabled", true);
                    $(".button-checkout").prop("disabled", true);
                },
                success: function(response) {
                    $("#input-url").prop("disabled", false);
                    $(".button-checkout").prop("disabled", false);

                    if (response.status === true) {
                        window.open(`${response.data.redirect_url}`, "_blank");
                    } else {
                        if ($("#is-autoclick").is(":checked")) {
                            $(this).click();
                        } else {
                            swal({
                                title: "Try Again",
                                text: response.message,
                                icon: "error",
                                button: "Tutup"
                            });
                        }
                    }
                },
                error: function(jqXHR, exception) {
                    const information = handleError(jqXHR, exception);

                    swal({
                        title: "Try Again",
                        text: information,
                        icon: "error",
                        button: "Tutup"
                    });

                    $("#input-url").prop("disabled", false);
                    $(".button-checkout").prop("disabled", false);
                }
            });
        });

        const handleRedirect = () => {
            var waLink = "";
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
                /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
                waLink = "https://wa.me/message/46JSCFTFN66JD1";
            } else {
                waLink = "https://web.whatsapp.com/send?phone=6282350952330";
            }

            window.location.href = waLink;
        }
    </script>
</body>

</html>