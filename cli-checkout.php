<?php
@date_default_timezone_set("Asia/Jakarta");

function json($status, $result)
{
    if ($status === true) {
        print_r(json_encode([
            "status" => true,
            "data" => $result
        ]));
    } else {
        print_r(json_encode([
            "status" => false,
            "message" => $result
        ]));
    }
}

function curl($type, $url, $headers = [], $data = [], $resultType = "ARRAY", $dataType = "ARRAY")
{
    // if ($url === "https://shopee.co.id/api/v2/checkout/get") {
    //     print_r($data);
    //     exit;
    // }
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ($type === "POST") {
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($dataType === "ARRAY") {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, true));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
    }

    $headers[] = "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $status = false;
        $result = "Error: " . curl_error($ch);
    } else {
        if ($result) {
            $status = true;
            if ($resultType === "ARRAY") {
                $result = json_decode($result, true);
            } else {
                $result = $result;
            }
        } else {
            $status = false;
            $result = "Something error";
        }
    }
    curl_close($ch);

    return [
        "status" => $status,
        "result" => $result
    ];
}

function handleRupiahFormat($value)
{
    if ($value) {
        $value = str_replace(substr($value, -5), "", $value);
    }

    return "Rp" . number_format($value, 2, ",", ".");
}

$files = array_diff(scandir(__DIR__ . "/configurations"), array('.', '..'));
if (!$files) {
    json(false, "Configuration file not found.");
    print_r("\n");
    exit;
} else {
    $defaultFileName = 'default.php';
    $files = array_diff($files, [$defaultFileName]);
    array_unshift($files, $defaultFileName);

    foreach ($files as $configurationKey => $file) {
        $configurationKey = $configurationKey + 1;
        print_r("\n" . $configurationKey . " - " . $file);
        $configuration[$configurationKey] = $file;
    }

    print_r("\n");
    do {
        print_r("Choose a configuration: ");
        if (count($argv) === 2) {
            print_r($currentConfiguration = $argv[1]);
            print_r("\n");
        } else {
            $inputConfiguration = fopen("php://stdin", "r");
            $currentConfiguration = trim(fgets($inputConfiguration));
        }

        if (empty($currentConfiguration)) {
            $currentConfiguration = "default.php";
            include __DIR__ . "/configurations/" . $currentConfiguration;

            $isWhile = false;
        } else {
            if (array_key_exists($currentConfiguration, $configuration)) {
                $currentConfiguration = $configuration[$currentConfiguration];
                include __DIR__ . "/configurations/" . $currentConfiguration;

                $isWhile = false;
            } else {
                print_r("    => ");
                json(false, "Configuration file not found.");
                print_r("\n");

                $isWhile = true;
            }
        }
    } while ($isWhile);
}

print_r("    => The configuration selected is " . $currentConfiguration);

$data = $_POST["data"];
$settings = $_POST["settings"];

$i = 1;
$j = 1;

if (date("Y-m-d H:i:s") > date("Y-m-d H:i:s", strtotime($data['flashsale_time']))) {
    print_r("\n    => End: " . date("Y-m-d H:i:s") . " > " . $data['flashsale_time']);

    print_r("\n\n");
    do {
    } while (true);
}

print_r("\n");
print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - ADDRESS");
print_r("\n    => ");

$res = curl("GET", "https://shopee.co.id/api/v4/account/address/get_user_address_list/", [
    'cookie: SPC_EC=' . $settings["spc_ec"] . ';'
]);

if ($res["status"] && @$res["result"]["data"]["addresses"]) {
    $addressKey = array_search($data["shipping"]["address_id"], array_column($res["result"]["data"]["addresses"], "id"));
    if (is_numeric($addressKey)) {
        $address = $res["result"]["data"]["addresses"][$addressKey];
        print_r("Name     : " . $address["name"]);
        print_r("\n       Address  : " . $address["address"]);
        print_r("\n       District : " . $address["district"]);
        print_r("\n       Regency  : " . $address["city"]);
        print_r("\n       Province : " . $address["state"]);
        print_r("\n       Zip Code : " . $address["zipcode"]);
    } else {
        json(false, "Address not found.");
        print_r("\n\n");
        exit;
    }
} else {
    json(false, "Something error.");
    print_r("\n\n");
    exit;
}

switch ($data["payment"]["channel"]["id"]) {
    case 8001400:
        $buyerFee = 0;
        break;
    case 8005200:
        $buyerFee = 100000000;
        break;
    case 8000700:
        $buyerFee = 75030000000;
        break;
    default:
        $buyerFee = 0;
}

if (is_numeric(@$data["payment"]["maximum"])) {
    $data["payment"]["maximum"] = $data["payment"]["maximum"] . "00000";
} else {
    $data["payment"]["maximum"] = NULL;
}

if (count($data["products"]) <= 1) {
    foreach ($data["products"] as $shopId => $product) {
    }

    $flashsaleTime = strtotime(date($data["flashsale_time"]));
    do {
        date_default_timezone_set("Asia/Jakarta");
    
        // print_r(date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) .  " - "  . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - WAITING\r");
    } while (date("Y-m-d H:i:s") < date("Y-m-d H:i:s", $flashsaleTime));
    // print_r("\n    => SUCCESS");

    do {
        // print_r("\n");
        // print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - MAKE AN ORDER");
        print_r("\n    => Total Payment: " . handleRupiahFormat($data["payment"]["total"]));
        print_r("\n    => ");
        $res = curl(
            "POST",
            "https://shopee.co.id/api/v2/checkout/place_order",
            [
                'cookie: SPC_EC=' . $settings["spc_ec"] . ';',
            ],
            '
            {
                "checkout_price_data": {
                    "total_payable": ' . $data["payment"]["total"] . '
                },
                "selected_payment_channel_data": {
                    "version": 2,
                    "option_info": "",
                    "channel_id": ' . $data["payment"]["channel"]["id"] . '
                },
                "shoporders": [{
                    "shop": {
                        "shopid": ' . $shopId . '
                    },
                    "items": [{
                        "itemid": ' . $product["item_id"] . ',
                        "modelid": ' . $product["model_id"] . ',
                        "quantity": 1,
                    }],
                    "buyer_address_data": {
                        "addressid": ' . $data["shipping"]["address_id"] . ',
                        "address_type": 0
                    },
                    "shipping_id": 1,
                    "selected_logistic_channelid": ' . $product["logistic"]["id"] . ',
                    "selected_preferred_delivery_time_option_id": ' . $product["logistic"]["delivery_time_option_id"] . '
                }],
                "shipping_orders": [{
                    "shipping_id": 1,
                    "shoporder_indexes": [0],
                    "selected_logistic_channelid": ' . $product["logistic"]["id"] . ',
                    "selected_preferred_delivery_time_option_id": ' . $product["logistic"]["delivery_time_option_id"] . ',
                    "buyer_address_data": {
                        "addressid": ' . $data["shipping"]["address_id"] . ',
                        "address_type": 0
                    }
                }],
                "timestamp": 1638293276
            }
            ',
            "ARRAY",
            "JSON"
        );
        if ($res["status"] === true && @array_key_exists("redirect_url", $res["result"])) {
            json(true, $res["result"]);

            print_r("\n");
            print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - END");
            print_r("\n    => SUCCESS");

            $j = 1337;
        } else {
            if (@array_key_exists("error", $res["result"])) {
                json(false, @$res["result"]["error_msg"]);
            } else {
                json(false, "Something error");
            }

            ++$j;
        }
    } while ($j <= 1337);
} else {
    include __DIR__ . '/cli-multiple.php';
}
