<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
date_default_timezone_set("Asia/Jakarta");

function json($status, $result)
{
    if ($status === true) {
        print_r(json_encode([
            "status" => true,
            "data" => $result
        ]));
    } else {
        print_r(json_encode([
            "status" => false,
            "message" => $result
        ]));
    }
}

function curl($type, $url, $headers = [], $data = [], $resultType = "ARRAY", $dataType = "JSON")
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ($type === "POST") {
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($dataType === "JSON") {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, true));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
    }

    $headers[] = "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $status = false;
        $result = "Error: " . curl_error($ch);
    } else {
        // print_r($result);
        // exit;

        if ($result) {
            $status = true;
            if ($resultType === "ARRAY") {
                $result = json_decode($result, true);
            } else {
                $result = $result;
            }
        } else {
            $status = false;
            $result = "Something error";
        }
    }
    curl_close($ch);

    return [
        "status" => $status,
        "result" => $result
    ];
}

function handleGetId($targetUrl)
{
    if (preg_match("/shopee.co.id\/product\/(.*)\/(.*)/i", str_replace("?", "&", $targetUrl), $explodeTargetUrl)) {;
        $shopId = $explodeTargetUrl[1];
        $itemId = $explodeTargetUrl[2];
    } else {
        $targetUrl = explode(".", $targetUrl);
        $shopId = $targetUrl[count($targetUrl) - 2];
        $itemId = $targetUrl[count($targetUrl) - 1];
    }

    return [
        "shop" => $shopId,
        "item" => $itemId
    ];
}

if (!empty($_GET["action"])) {
    $settings["CSRF_TOKEN"] = "YyXlTqL8UDtIFSIsmNJyLIQTj9H2qsS8";
    $settings["SPC_SI"] = "mall.kSr1yhNZMVWOo3e4Q0llbNI954BGfiDm";
    $settings["_GCL_AW"] = "GCL.1597493060.CjwKCAjwj975BRBUEiwA4whRBxLYWWf5PG1osquPtphNm-lyiLVp_D2d2Xt5UDVqL6CJJPLSlcQy-BoCcrYQAvD_BwE";

    if ($_GET["action"] === "get-information") {
        if (!empty($_POST["url"])) {
            $id = handleGetId($_POST["url"]);
            $res = curl("GET", "https://shopee.co.id/api/v2/item/get?shopid={$id["shop"]}&itemid={$id["item"]}", [
                "If-None-Match-: 55b03-969681079d083261e39752ddcc2587e9",
            ]);
            if ($res["result"]) {
                json($res["status"], $res["result"]["item"]);
            } else {
                json(false, "Try again.");
            }
        } else {
            json(false, "Data sent is incomplete.");
        }
    } else if ($_GET["action"] === "add-to-cart") {
        if (
            !empty($_POST["shop"]) && is_numeric($_POST["shop"])
            && !empty($_POST["item"]) && is_numeric($_POST["item"])
            && !empty($_POST["model"]) && is_numeric($_POST["model"])
            && !empty($_POST["csrf_token"])
            && !empty($_POST["spc_si"])
        ) {
            $data = [
                "quantity" => 1,
                "shopid" => (int) $_POST["shop"],
                "itemid" => (int) $_POST["item"],
                "modelid" => (int) $_POST["model"],
                // "checkout" => true, //optional
                // "update_checkout_only" => false, //optional
                // "source" => "", //optional
                // "client_source" => 1, //optional
                // "donot_add_quantity" => false,
            ];

            $settings = [
                "CSRF_TOKEN" => $_POST["csrf_token"],
                "SPC_SI" => $_POST["spc_si"]
            ];

            $res = curl("POST", "https://shopee.co.id/api/v2/cart/add_to_cart", [
                'Referer: https://shopee.co.id',
                'X-Csrftoken: ' . $settings["CSRF_TOKEN"],
                'Cookie: csrftoken=' . $settings["CSRF_TOKEN"] . '; SPC_SI=' . $settings["SPC_SI"] . ';'
            ], $data);
            if ($res["status"] === true && @array_key_exists("error", $res["result"]) && $res["result"]["error"] === 0) {
                json($res["status"], $res["result"]);
            } else {
                if (array_key_exists("error", $res["result"]) && $res["result"]["error"] === 6) {
                    json(false, "Stock not available");
                } else {
                    json(false, "Something error");
                }
            }
        } else {
            json(false, "Data sent is incomplete.");
        }
    } else if ($_GET["action"] === "checkout") {
        if (
            !empty($_POST["shop"]) && is_numeric($_POST["shop"])
            && !empty($_POST["item"]) && is_numeric($_POST["item"])
            && !empty($_POST["model"]) && is_numeric($_POST["model"])
            && !empty($_POST["csrf_token"])
            && !empty($_POST["spc_si"])
        ) {
            $data = [
                "shop" => (int) $_POST["shop"],
                "item" => (int) $_POST["item"],
                "model" => (int) $_POST["model"]
            ];

            $settings = [
                "CSRF_TOKEN" => $_POST["csrf_token"],
                "SPC_SI" => $_POST["spc_si"]
            ];

            $res = curl("POST", "https://shopee.co.id/api/v2/cart/add_to_cart", [
                'Referer: https://shopee.co.id',
                'X-Csrftoken: ' . $settings["CSRF_TOKEN"],
                'Cookie: csrftoken=' . $settings["CSRF_TOKEN"] . '; SPC_SI=' . $settings["SPC_SI"] . ';'
            ], [
                "quantity" => 1,
                "shopid" => $data["shop"],
                "itemid" => $data["item"],
                "modelid" => $data["model"],
                // "checkout" => true, //optional
                // "update_checkout_only" => false, //optional
                // "source" => "", //optional
                // "client_source" => 1, //optional
                // "donot_add_quantity" => false,
            ]);
            if ($res["status"] === true && @array_key_exists("error", $res["result"]) && $res["result"]["error"] === 0) {
                $res = curl("GET", "https://shopee.co.id/api/v1/account_info/?from_wallet=true&need_cart=1&skip_address=1", [
                    'Referer: https://shopee.co.id',
                    'X-Csrftoken: ' . $settings["CSRF_TOKEN"],
                    'Cookie: csrftoken=' . $settings["CSRF_TOKEN"] . '; SPC_SI=' . $settings["SPC_SI"] . ';'
                ]);
                if ($res["status"] === true && @array_key_exists("recent_cart_items", $res["result"]) && $res["result"]["recent_cart_items"]) {
                    $accountInfo = $res["result"];
                    $res = curl("POST", "https://shopee.co.id/api/v2/checkout/get", [
                        'Referer: https://shopee.co.id',
                        'X-Csrftoken: ' . $settings["CSRF_TOKEN"],
                        'Cookie: csrftoken=' . $settings["CSRF_TOKEN"] . '; SPC_SI=' . $settings["SPC_SI"] . ';'
                    ], [
                        "shoporders" => [
                            [
                                "shop" => [
                                    "shopid" => $data["shop"]
                                ],
                                "items" => [
                                    [
                                        "itemid" => $data["item"],
                                        "modelid" => $data["model"],
                                        "item_group_id" => @$accountInfo["recent_cart_items"]["item_group_id"] ?? 0,
                                        "quantity" => 1
                                    ]
                                ]
                            ]
                        ]
                    ], "JSON");
                    if ($res["status"] === true) {
                        $checkout = $res["result"];
                        $res = curl("POST", "https://shopee.co.id/api/v2/checkout/place_order", [
                            'Referer: https://shopee.co.id',
                            'X-Csrftoken: ' . $settings["CSRF_TOKEN"],
                            'Cookie: csrftoken=' . $settings["CSRF_TOKEN"] . '; SPC_SI=' . $settings["SPC_SI"] . ';'
                        ], $checkout, "ARRAY", "STRING");
                        if ($res["status"] === true && @array_key_exists("redirect_url", $res["result"])) {
                            json(true, $res["result"]);
                        } else {
                            if (@array_key_exists("error", $res["result"])) {
                                json(false, $res["result"]["error_msg"]);
                            } else {
                                json(false, "Something error");
                            }
                        }
                    } else {
                        if (@array_key_exists("error", $res["result"])) {
                            json(false, $res["result"]["error_msg"]);
                        } else {
                            json(false, "Something error");
                        }
                    }
                } else {
                    if (@array_key_exists("recent_cart_items", $res["result"])) {
                        json(false, "The shopping cart is still empty");
                    } else {
                        json(false, "Something error");
                    }
                }
            } else {
                if (@array_key_exists("error", $res["result"]) && $res["result"]["error"] === 6) {
                    json(false, "Stock not available");
                } else {
                    json(false, "Something error");
                }
            }
        } else {
            json(false, "Data sent is incomplete.");
        }
    } else {
        json(false, "Request not found.");
    }
} else {
    json(true, "Made with love by Alvriyanto Azis.");
}
