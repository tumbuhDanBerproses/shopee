<?php

$_POST = [
    "settings" => [
        "spc_ec" => "d29tUDlvcnFGSU9yOTBmMBqmaIYrVs+trSfjSETepdRwcm1LetaPnY3RTBFBn5xM6geuhiZpUDaIUoqEkaSCaKFnwEtbMmZrRW2+n/o/chEiXfG5TATtLeNB8y4IkWtfJPXWyLEEBuBXIdqIkKXQfsGxbDrUjU1ZEImP4gbnNwQ=",
        "csrf"   => "mi6cJfV9O8QRgObZ3tKJ8D60rjaaNX5o",
    ],
    "data" => [
        "products" => [
            /*
            jnt: [
                jemari: 80024,
                truck: 80008,
            ],
            siCepatGokil: 80031,
            instant: 8000,
            sameDay: 8001,
            nextDay: 8002,
            regular: 8003,
            hemat: 8005,
            */

            154384940 => [
                "item_id" => 6983580335,
                "model_id" => 134272039368,
                "logistic" => [
                    "id" => 8003,
                    "delivery_time_option_id" => 0
                ]
            ],
        ],
        "shipping" => [
            // "address_id" => 6296848 // tegal
            // "address_id" => 99447849 // jogja / afif
            // "address_id" => 56476999 // jogja / wahyu
            // "address_id" => 107336060 // wonogiri
            "address_id" => 80330090 // jakarta
        ],
        "payment" => [
            "channel" => [
                "id" => 8001400, // shopeePay: 8001400, bank: 8005200, alfamart: 8003200, indomaret: 8003001
                "option_info" => NULL // shopeePay: NULL, bca: 89052001, bri: 89052004, bni: 89052003
            ],
            // "channel" => [
            //     "id" => 8005200,
            //     "option_info" => 89052001
            // ],
        ],
        "flashsale_time" => date("2022-12-26 00:00:00") // NULL or date("2021-07-08 02:29:00")
        // "flashsale_time" => NULL // NULL or date("2021-07-08 02:29:00")
    ]
];
