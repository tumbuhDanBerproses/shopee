<?php

ksort($data["products"]);

print_r("\n");
print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - ADD TO CHART");
$shippings = [];
$shippingFee = 0;
$flashsaleTime = strtotime(date('Y-m-d H:i:s'));
$shopOrders = [];
$shippingOrders = [];
$shippingId = 0;
foreach ($data["products"] as $shopId => $product) {
    print_r("\n    => ");
    $res = curl("GET", "https://shopee.co.id/api/v2/item/get?shopid=" . $shopId . "&itemid=" . $data["products"][$shopId]["item_id"]);
    if ($res["status"] && @$res["result"]["item"]) {
        $item = $res["result"]["item"];
        if ($data["flashsale_time"]) {
            $flashsaleTimeTemp = strtotime(date($data["flashsale_time"]));
        } else {
            if (@$item["upcoming_flash_sale"]) {
                $flashsaleTimeTemp = $item["upcoming_flash_sale"]["start_time"];
            } else if (@$item["flash_sale"]) {
                $flashsaleTimeTemp = $item["flash_sale"]["start_time"];
            } else {
                $flashsaleTimeTemp = strtotime(date('Y-m-d H:i:s'));
            }
        }

        if ($flashsaleTimeTemp >= $flashsaleTime) {
            $flashsaleTime = $flashsaleTimeTemp;
        }

        print_r($item["name"]);
    } else {
        json(false, "Product not defined.");
        print_r("\n\n");
        exit;
    }

    $res = curl("GET", "https://shopee.co.id/api/v0/shop/" . $shopId . "/item/" . $data["products"][$shopId]["item_id"] . "/shipping_info_to_address/?state=" . urlencode($address["state"]) . "&city=" . urlencode($address["city"]) . "&district=" . urlencode($address["district"]));
    print_r("\n           ");
    if ($res["status"] && @$res["result"]["shipping_infos"]) {
        $tempShippingCheapest = 133333333333337;
        foreach ($res["result"]["shipping_infos"] as $tempShippingId => $tempShipping) {
            if ($tempShipping["delivery_info"]["has_edt"]) {
                if ($product["logistic"]["id"]) {
                    if ($tempShipping["channel"]["channelid"] === $product["logistic"]["id"]) {
                        $shippings[$shopId] = $tempShipping;
                    }
                } else {
                    if ($tempShipping["shop_promo_only_cost_info"]["estimated_shipping_fee"] < $tempShippingCheapest) {
                        $tempShippingCheapest = $tempShipping["shop_promo_only_cost_info"]["estimated_shipping_fee"];
                        $shippings[$shopId] = $tempShipping;
                    }
                }
            }
        }

        if (array_key_exists($shopId, $shippings)) {
            $shippingFee += $shippings[$shopId]["shop_promo_only_cost_info"]["estimated_shipping_fee"];

            print_r($shippings[$shopId]["channel"]["channelid"] . ' : - ' . $shippings[$shopId]["channel"]["name"]);
            print_r("\n                   - " . date("Y-m-d", $shippings[$shopId]["delivery_info"]["estimated_delivery_date_from"]) . " - " . date("Y-m-d", $shippings[$shopId]["delivery_info"]["estimated_delivery_date_to"]));
            print_r("\n                   - " . handleRupiahFormat($shippings[$shopId]["shop_promo_only_cost_info"]["estimated_shipping_fee"]));
        } else {
            json(false, "The shipping option was not found.");
            print_r("\n\n");
            exit;
        }
    } else {
        print_r("\n           ");
        json(false, "Logistic service type not found.");
        print_r("\n\n");
        exit;
    }

    print_r("\n           Price : ");
    $res = curl("POST", "https://shopee.co.id/api/v2/cart/add_to_cart", [
        'content-type: application/json',
        'referer: https://shopee.co.id',
        'x-csrftoken: ' . $settings["csrf_token"],
        'cookie: csrftoken=' . $settings["csrf_token"] . '; SPC_SI=' . $settings["spc_si"] . ';'
    ], [
        "quantity" => 1,
        "shopid" => $shopId,
        "itemid" => $product["item_id"],
        "modelid" => $product["model_id"],
        "update_checkout_only" => true
    ]);
    if (@$res["result"]["error"] === 0) {
        $cartItem = $res["result"]["data"]["cart_item"];
        $data["products"][$shopId]["item_id"] = $cartItem["itemid"];
        $data["products"][$shopId]["model_id"] = $cartItem["modelid"];
        $data["products"][$shopId]["item_group_id"] = $cartItem["item_group_id"];

        print_r(handleRupiahFormat($res["result"]["data"]["cart_item"]["price"]));

        $shopOrders[] = '{
            "shop": {
                "shopid": ' . $shopId . '
            },
            "items": [
                {
                    "itemid": ' . $product["item_id"] . ',
                    "modelid": ' . $product["model_id"] . ',
                    "item_group_id": ' . $data["products"][$shopId]["item_group_id"] . ',
                    "quantity": 1
                }
            ],
            "buyer_address_data": {
                "address_type": 0,
                "addressid": ' . $data["shipping"]["address_id"] . '
            },
            "selected_logistic_channelid": ' . $shippings[$shopId]["channel"]["channelid"] . ',
            "shipping_id": ' . ($shippingId + 1) . ',
            "selected_preferred_delivery_time_option_id": ' . $product["logistic"]["delivery_time_option_id"] . '
        }';

        $shippingOrders[] = '{
            "sync": true,
            "buyer_address_data": {
                "address_type": 0,
                "addressid": ' . $data["shipping"]["address_id"] . '
            },
            "selected_logistic_channelid": ' . $shippings[$shopId]["channel"]["channelid"] . ',
            "shipping_id": ' . ($shippingId + 1) . ',
            "shoporder_indexes": [
                ' . $shippingId . '
            ],
            "selected_preferred_delivery_time_option_id": ' . $product["logistic"]["delivery_time_option_id"] . '
        }';

        ++$shippingId;
    } else {
        json(false, "Cannot add to cart.");
    }
}

if (count($data["products"]) === count($shippings)) {
    do {
        print_r("\n");
        print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - ACCOUNT INFO");
        print_r("\n    => ");
        $res = curl(
            "POST",
            "https://shopee.co.id/api/v2/checkout/get",
            [
                'content-type: application/json',
                'referer: https://shopee.co.id',
                'x-csrftoken: ' . $settings["csrf_token"],
                'cookie: csrftoken=' . $settings["csrf_token"] . '; SPC_SI=' . $settings["spc_si"] . ';'
            ],
            '
            {
                "timestamp": ' . strtotime(date("Y-m-d H:i:00")) . ',
                "shoporders": [
                    ' . implode(",", $shopOrders) . '
                ],
                "selected_payment_channel_data": {
                    "channel_id": 8005200,
                    "channel_item_option_info": {
                        "option_info": "89052001"
                    },
                    "version": 2
                },
                "promotion_data": {
                    "use_coins": ' . ($data["payment"]["use_coin"] ? 'true' : 'false') . '
                },
                "shipping_orders": [
                    ' . implode(",", $shippingOrders) . '
                ]
            }
            ',
            "ARRAY",
            "JSON"
        );
        if (array_key_exists("payment_channel_info", $res["result"])) {
            $shopeePay = handleRupiahFormat($res["result"]["payment_channel_info"]["channels"][0]["balance"]);
            $timesTamp = @$res["result"]["timestamp"];

            $isWhile = false;
        } else {
            json(false, $res["result"]);

            ++$j;
            $isWhile = true;
        }
    } while ($isWhile);

    print_r("Payment   : " . $data["payment"]["channel"]["id"] . ($data["payment"]["channel"]["option_info"] ? " - " . $data["payment"]["channel"]["option_info"] : ''));
    print_r("\n       Time      : " . date("Y-m-d H:i:s", $flashsaleTime) . " - " . $flashsaleTime);
    print_r("\n       ShopeePay : " . $shopeePay);
    print_r("\n       Ongkir    : " . handleRupiahFormat($shippingFee));
    print_r("\n       Fee       : " . handleRupiahFormat($buyerFee));

    print_r("\n\n");

    do {
        print_r(date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) .  " - "  . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - WAITING\r");
    } while (date("Y-m-d H:i:s") < date("Y-m-d H:i:s", $flashsaleTime));
    print_r("\n    => SUCCESS");

    print_r("\n");
    print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - REFRESH CHART");
    $shopOrders = [];
    $shippingOrders = [];
    $totalPayment = 0;
    $shippingId = 0;
    foreach ($data["products"] as $shopId => $product) {
        print_r("\n    => ");
        $res = curl("POST", "https://shopee.co.id/api/v2/cart/add_to_cart", [
            'content-type: application/json',
            'referer: https://shopee.co.id',
            'x-csrftoken: ' . $settings["csrf_token"],
            'cookie: csrftoken=' . $settings["csrf_token"] . '; SPC_SI=' . $settings["spc_si"] . ';'
        ], [
            "quantity" => 1,
            "shopid" => $shopId,
            "itemid" => $product["item_id"],
            "modelid" => $product["model_id"]
        ]);
        if (@$res["result"]["error"] === 0) {
            print_r("SUCCESS : " . $shopId . " - " . $product["item_id"] . " - " . $product["model_id"] . " - " . handleRupiahFormat($res["result"]["data"]["cart_item"]["price"]));

            $totalPayment += $res["result"]["data"]["cart_item"]["price"] + $shippings[$shopId]["shop_promo_only_cost_info"]["estimated_shipping_fee"];
            $data["products"][$shopId]["item_group_id"] = $res["result"]["data"]["cart_item"]["item_group_id"];

            $shippingOrders[] = '{
                "shipping_id": ' . ($shippingId + 1) . ',
                "selected_logistic_channelid": ' . $shippings[$shopId]["channel"]["channelid"] . ',
                "buyer_address_data": {
                    "address_type": 0,
                    "addressid": ' . $data["shipping"]["address_id"] . '
                },
                "shoporder_indexes": [
                    ' . $shippingId . '
                ],
                "selected_preferred_delivery_time_option_id": ' . $product["logistic"]["delivery_time_option_id"] . '
            }';

            $shopOrders[] = '{
                "shop": {
                    "shopid": ' . $shopId . '
                },
                "shipping_id": ' . ($shippingId + 1) . ',
                "items": [
                    {
                        "itemid": ' . $product["item_id"] . ',
                        "quantity": 1,
                        "modelid": ' . $product["model_id"] . ',
                        "item_group_id": ' . $data["products"][$shopId]["item_group_id"] . '
                    }
                ],
                "selected_logistic_channelid": ' . $shippings[$shopId]["channel"]["channelid"] . ',
                "buyer_address_data": {
                    "address_type": 0,
                    "addressid": ' . $data["shipping"]["address_id"] . '
                },
                "selected_preferred_delivery_time_option_id": ' . $product["logistic"]["delivery_time_option_id"] . '
            }';

            ++$shippingId;
        } else {
            print_r("FAILED  : ");
            json(false, "Cannot add to cart.");
        }
    }

    if ($shopOrders && $shippingOrders) {
        $totalPayment += $buyerFee;

        do {
            print_r("\n");
            print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - MAKE AN ORDER");
            print_r("\n    => ");
            $res = curl(
                "POST",
                "https://shopee.co.id/api/v2/checkout/place_order",
                [
                    'content-type: application/json',
                    'referer: https://shopee.co.id',
                    'x-csrftoken: ' . $settings["csrf_token"],
                    'cookie: csrftoken=' . $settings["csrf_token"] . '; SPC_SI=' . $settings["spc_si"] . ';'
                ],
                '{
                    "shipping_orders": [
                        ' . implode(",", $shippingOrders) . '
                    ],
                    "timestamp": ' . (@$timesTamp ?? 1605891600) . ',
                    "checkout_price_data": {
                        "total_payable": ' . $totalPayment . '
                    },
                    "selected_payment_channel_data": {
                        "channel_id": ' . $data["payment"]["channel"]["id"] . ',
                        "channel_item_option_info": {' . ($data["payment"]["channel"]["option_info"] ? '
                            "option_info": "' . $data["payment"]["channel"]["option_info"] . '"
                        ' : '') . '},
                        "version": 2
                    },
                    "shoporders": [
                        ' . implode(",", $shopOrders) . '
                    ]
                }',
                "ARRAY",
                "JSON"
            );
            if ($res["status"] === true && @array_key_exists("redirect_url", $res["result"])) {
                json(true, $res["result"]);

                print_r("\n");
                print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - END");
                print_r("\n    => SUCCESS");

                $isWhile = false;
            } else {
                if (@array_key_exists("error", $res["result"])) {
                    json(false, @$res["result"]["error_msg"]);
                } else {
                    json(false, "Something error");
                }

                ++$j;
                $isWhile = true;
            }
        } while ($isWhile);
    } else {
        print_r("\n");
        print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - END");
        print_r("\n    => FAILED  : ");
        json(false, "Orders not found.");
    }
} else {
    print_r("\n    => FAILED  : ");
    json(false, "There was a shipment that was not found on the product.");
}
