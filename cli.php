<?php
@date_default_timezone_set("Asia/Jakarta");

function json($status, $result)
{
    if ($status === true) {
        print_r(json_encode([
            "status" => true,
            "data" => $result
        ]));
    } else {
        print_r(json_encode([
            "status" => false,
            "message" => $result
        ]));
    }
}

$cookies = [];
function curl($type, $url, $headers = [], $cookies = [], $data = [], $resultType = "ARRAY", $dataType = "ARRAY")
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if ($type === "POST") {
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($dataType === "ARRAY") {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, true));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
    } else {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    }

    // set headers
    $headersTenp = [
        'authority'             => 'shopee.co.id',
        'accept'                => '*/*',
        'accept-language'       => 'en-US,en;q=0.9,es;q=0.8,id;q=0.7',
        'dnt'                   => '1',
        'if-none-match-'        => '55b03-7404008ec63e78568f7cc4f6f8a5ecc9',
        'sec-ch-ua'             => '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile'      => '?0',
        'sec-ch-ua-platform'    => '"Linux"',
        'sec-fetch-dest'        => 'empty',
        'sec-fetch-mode'        => 'cors',
        'sec-fetch-site'        => 'same-origin',
        'user-agent'            => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'x-api-source'          => 'pc',
        'x-requested-with'      => 'XMLHttpRequest',
        'x-shopee-language'     => 'id',
        'Accept-Encoding'       => 'gzip',
    ];
    if ($headers) {
        foreach($headers AS $headerKey => $headerValue) {
            $headersTenp[$headerKey] = $headerValue;
        }
    }
    $headers = $headersTenp;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // set cookies
    if ($GLOBALS['cookies']) {
        $cookiesTemp = [];
        foreach($GLOBALS['cookies'] as $cookie) {
            $cookiesTemp[] = $cookie;
        }

        if ($cookies && $cookiesTemp) {
            $cookies = array_merge($cookies, $cookiesTemp);
        } else if (!$cookies && $cookiesTemp) {
            $cookies = $cookiesTemp;
        }
    }
    if ($cookies) {
        $cookies = join('; ', $cookies);
        curl_setopt($ch, CURLOPT_COOKIE, $cookies);
    }

    // get result
    $response = curl_exec($ch);

    // set cookies to global variable
    if (count($GLOBALS['cookies']) == 0) {
        curl_setopt($ch, CURLOPT_HEADER, true);
        $cookieTemp         = curl_exec($ch);
        $cookieSize         = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $cookieTemp         = substr($cookieTemp, 0, $cookieSize);

        preg_match_all('/set-cookie\: (.*)\; Path/', $cookieTemp, $matches);
        if ($matches[1]) {
            $GLOBALS['cookies'] = $matches[1];
        }
    }

    if (curl_errno($ch)) {
        $status = false;
        $response = "Error: " . curl_error($ch);
    } else {
        if ($response) {
            $status = true;
            if ($resultType === "ARRAY") {
                $response = json_decode($response, true);
            } else {
                $response = $response;
            }
        } else {
            $status = false;
            $response = "Something error";
        }
    }
    curl_close($ch);

    return [
        "status" => $status,
        "result" => $response
    ];
}

function handleRupiahFormat($value)
{
    if ($value) {
        $value = str_replace(substr($value, -5), "", $value);
    }

    return "Rp" . number_format($value, 2, ",", ".");
}

$files = array_diff(scandir(__DIR__ . "/configurations"), array('.', '..'));
if (!$files) {
    json(false, "Configuration file not found.");
    print_r("\n");
    exit;
} else {
    $defaultFileName = 'default.php';
    $files = array_diff($files, [$defaultFileName]);
    array_unshift($files, $defaultFileName);

    foreach ($files as $configurationKey => $file) {
        $configurationKey = $configurationKey + 1;
        print_r("\n" . $configurationKey . " - " . $file);
        $configuration[$configurationKey] = $file;
    }

    print_r("\n");
    do {
        print_r("Choose a configuration: ");
        if (count($argv) === 2) {
            print_r($currentConfiguration = $argv[1]);
            print_r("\n");
        } else {
            $inputConfiguration = fopen("php://stdin", "r");
            $currentConfiguration = trim(fgets($inputConfiguration));
        }

        if (empty($currentConfiguration)) {
            $currentConfiguration = "default.php";
            include __DIR__ . "/configurations/" . $currentConfiguration;

            $isWhile = false;
        } else {
            if (array_key_exists($currentConfiguration, $configuration)) {
                $currentConfiguration = $configuration[$currentConfiguration];
                include __DIR__ . "/configurations/" . $currentConfiguration;

                $isWhile = false;
            } else {
                print_r("    => ");
                json(false, "Configuration file not found.");
                print_r("\n");

                $isWhile = true;
            }
        }
    } while ($isWhile);
}

print_r("    => The configuration selected is " . $currentConfiguration);

$data = $_POST["data"];
$settings = $_POST["settings"];

$i = 1;
$j = 1;

if ($data['flashsale_time'] != NULL && date("Y-m-d H:i:s") > date("Y-m-d H:i:s", strtotime($data['flashsale_time']))) {
    print_r("\n    => End: " . date("Y-m-d H:i:s") . " > " . $data['flashsale_time']);

    print_r("\n\n");
    do {
    } while (true);
}

print_r("\n");
print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - ADDRESS");
print_r("\n    => ");

$res = curl("GET", "https://shopee.co.id/api/v4/account/address/get_user_address_list?with_warehouse_whitelist_status=true", [
    'referer'   => 'https://shopee.co.id/user/account/address',
], [
    'csrftoken=' . $settings["csrf"],
    'SPC_EC=' . $settings["spc_ec"],
]);
if ($res["status"] && @$res["result"]["data"]["addresses"]) {
    $addressKey = array_search($data["shipping"]["address_id"], array_column($res["result"]["data"]["addresses"], "id"));
    if (is_numeric($addressKey)) {
        $address = $res["result"]["data"]["addresses"][$addressKey];
        print_r("Name     : " . $address["name"]);
        print_r("\n       Address  : " . $address["address"]);
        print_r("\n       District : " . $address["district"]);
        print_r("\n       Regency  : " . $address["city"]);
        print_r("\n       Province : " . $address["state"]);
        print_r("\n       Zip Code : " . $address["zipcode"]);
    } else {
        json(false, "Address not found.");
        print_r("\n\n");
        exit;
    }
} else {
    json(false, "Something error.");
    print_r("\n\n");
    exit;
}

switch ($data["payment"]["channel"]["id"]) {
    case 8001400:
        $buyerFee = 0;
        break;
    case 8005200:
        $buyerFee = 200000000;
        break;
    case 8000700:
        $buyerFee = 75030000000;
        break;
    default:
        $buyerFee = 0;
}

if (is_numeric(@$data["payment"]["maximum"])) {
    $data["payment"]["maximum"] = $data["payment"]["maximum"] . "00000";
} else {
    $data["payment"]["maximum"] = NULL;
}

if (count($data["products"]) <= 1) {
    foreach ($data["products"] as $shopId => $product) {
    }

    include __DIR__ . '/cli-single.php';
} else {
    include __DIR__ . '/cli-multiple.php';
}
