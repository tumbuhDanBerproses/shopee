<?php
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://shopee.co.id/api/v4/checkout/place_order');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'authority'             => 'shopee.co.id',
    'accept'                => 'application/json',
    'accept-language'       => 'en-US,en;q=0.9,es;q=0.8,id;q=0.7',
    'content-type'          => 'application/json',
    'dnt'                   => '1',
    'origin'                => 'https://shopee.co.id',
    'referer'               => 'https://shopee.co.id/checkout/?state=H8KLCAAAAAAAAAPChVXDm8KSw6I4DMO9wpc8w7MQCMOQwqR%2FZWvDimVsBcK8w63DmBlfGMKoKcO%2BfSRfwpLDgMO2w5TDkg%2FCnUjCsiLDqcOoHMO%2FbsKYGELDs8O5T8KHwr%2FCj8Kuw7%2FCsWnCghrDgQc%2BTsONw6fDtsO4wrFrwrs9w75tGn%2FCtcKTdRLCnMOHw6jDn8OpwrXDucOMw7%2FClcOEw5DDg8K%2BO8Otw7t9w7vDnDQqw4DCmMKjw6jCicK8w4fDvsOUHU5tw5cdNsONaCXDqHTCpMObw68of8OfHU%2FCm8OmZ8OkJsKow7BAw7vCpsOhUjJrwpgEwq5ZCsOddh%2FDh2PCt8ODw5zCnhXCp8KPZ0bDucKbw4%2FCgWsPw7nCs8Osw6JswpzDksKZw6bCsMOdwrfDh23Dn39gF8O7w5N%2BwrvDrw8NRhkfHTcCwqhCbFdcwrkxwqAZw5zChcKOXsOdwoApM8OYw5TCmsKNTkDDisOVw6JYw6wXGMOMw5pgwoM%2BTjjCi8Ogw5nDoADCmMKDEMKdw7HCpcKOw6cPwprClcKaJmUuwrnDtmQBDSLCgGQTf8KMYALCq8KfwpU8cMO6w5gNB8KrLH4Ae8K0U8OAw4dSR8OTLDVSwrpTw5tiX1QQw5xDwq3DtcKJRU3DjsKONh3CrDnCowcmwqzCmkvDmzTCqcOewrnCusKbwo3DogpubsO4L8Oew5zDv8O3TsKBYMOmGsKlw7LDvMKswrFDB8OcUyMmasK9acOOVMO5w7LCicORX3LCuAbDrgwbwq0DVmzDmMKCwoPCn1E5w4xxBsK2w7RcwoDCmsKdfhrCqzfCucKod8ONw4Ngw51Yw6sqR2g5w59Mw7gowr7DmMOaw4EQwo7CuhBlTDwGw4vDuDTDqcOHS2RFGMKHw6FvLENawoEqw6klw5zClFhWwqjCvsOKMsKjw7w6w6AEw4FNTsKZwpDDrcOBwprCi8KMwobCnTUXX2d7w4%2FDlnN8YHV1YVZIZ8OHwrzCplfCjkPDhMKCB8Knw4BIwr98XHnCnMOLA13Cl8KLwobCuTkwwoTDk8K7wpVrbX9RwpbClG7DqcO0w6shSsOCwoxnwprCn8KNwoHDgcOIwpUuwrgJw64CC8KPCcOSwqYIwq1Sw4Fyw70GwrdVE8OEw6J3w4gSwqQXwpALwp3CkV44w6ZXM34sw7DDu8KcJz3Di1LChXR2WsKYVyJywrdLVsODw4fCssKww5PDlRpgJsKOZ0I2w7PCusKeXgvDncODwojDpjPCuAh1w7DCqD8OwrzCn2lWw54zOcK7wq5tw7t2U8KNw4tUwqjDlGLCrF%2FCq8KawqDDrUXDucKgRMOdw6pCw7PDrl1IViLCjMO9ScK4Jw1rX8Okw4XDgQDCjlhCIsKLwqLCgsOwwqPCnsKzKikZwpPDvw3Dt8OaZgAzw6gYw6bChVNrWUIMR37CoQIowoQoEcO1wqDCtF7Dr8Oqwottw5AcaX7DnMK%2FRmbCjcONwoDCjMOcYEYkP8Kyw67CvMKQMcO3wrs%2BwpRnwopCw7kLV8O1aknDpsOqwrhLwoPCtDsowofDjMKSwqzCoh7DlcOtISnDjsO5w6LChMOww41EfcKuFht5wokcwq3DoMKOw6AIFcOkEsOQwo0TbMOgwrjDtcObfsOXw6bDnwrCo8OqRsO1GkjDl8OwwozCsMKROMOewrfDv8KNwq7DnlXDlsOvwpLCvRzCph1CPcKgwo3CnncKwrdgTMOAZMK2RR%2FCsG9ywrzDpE3ClwPCiXXCkjUFM8K0AsOBV3gdcUfDm1BuwpDDqsKkdcKDcsKHwqDCrkvCgHE5wplvWsKkw4TCqsORwpY4YiTDigtdw51%2Bw6XCqcKLRMKzw6XDsl8swpPCoMKcb8KJRMKscDdsAChpw4jDosOBZcOFJMOrdhnDhHzCicK%2FdsK5wpjDnyDDuFvDmMO3w74bfzNkWMOnw4nDr2bDtMKfw48%2Fw45JwrJxQQkAAA%3D%3D',
    'sec-ch-ua'             => '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
    'sec-ch-ua-mobile'      => '?0',
    'sec-ch-ua-platform'    => '"Linux"',
    'sec-fetch-dest'        => 'empty',
    'sec-fetch-mode'        => 'cors',
    'sec-fetch-site'        => 'same-origin',
    'user-agent'            => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'x-api-source'          => 'pc',
    'x-csrftoken'           => 'mi6cJfV9O8QRgObZ3tKJ8D60rjaaNX5o',
    'x-sap-access-t'        => '1672035487',
    'x-shopee-language'     => 'id',
    'Accept-Encoding'       => 'gzip',
]);
curl_setopt($ch, CURLOPT_COOKIE, 'SPC_EC=d29tUDlvcnFGSU9yOTBmMBqmaIYrVs+trSfjSETepdRwcm1LetaPnY3RTBFBn5xM6geuhiZpUDaIUoqEkaSCaKFnwEtbMmZrRW2+n/o/chEiXfG5TATtLeNB8y4IkWtfJPXWyLEEBuBXIdqIkKXQfsGxbDrUjU1ZEImP4gbnNwQ=');
curl_setopt($ch, CURLOPT_POSTFIELDS, '{"client_id":0,"cart_type":0,"timestamp":1672034343,"checkout_price_data":{"merchandise_subtotal":1920000000,"shipping_subtotal_before_discount":900000000,"shipping_discount_subtotal":0,"shipping_subtotal":900000000,"tax_payable":0,"tax_exemption":0,"custom_tax_subtotal":0,"promocode_applied":null,"credit_card_promotion":null,"shopee_coins_redeemed":null,"group_buy_discount":0,"bundle_deals_discount":null,"price_adjustment":null,"buyer_txn_fee":0,"buyer_service_fee":100000000,"insurance_subtotal":0,"insurance_before_discount_subtotal":0,"insurance_discount_subtotal":0,"vat_subtotal":0,"total_payable":2920000000},"order_update_info":{},"dropshipping_info":{"enabled":false,"name":"","phone_number":""},"promotion_data":{"can_use_coins":true,"use_coins":false,"platform_vouchers":[],"free_shipping_voucher_info":{"free_shipping_voucher_id":0,"free_shipping_voucher_code":"","disabled_reason":null,"banner_info":{"msg":"","learn_more_msg":""},"required_be_channel_ids":[],"required_spm_channels":[]},"highlighted_platform_voucher_type":-1,"shop_voucher_entrances":[{"shopid":154384940,"status":true}],"applied_voucher_code":null,"voucher_code":null,"voucher_info":{"coin_earned":0,"voucher_code":null,"coin_percentage":0,"discount_percentage":0,"discount_value":0,"promotionid":0,"reward_type":0,"used_price":0},"invalid_message":"","price_discount":0,"coin_info":{"coin_offset":1000000,"coin_used":10,"coin_earn_by_voucher":0,"coin_earn":0},"card_promotion_id":null,"card_promotion_enabled":false,"promotion_msg":""},"selected_payment_channel_data":{"version":2,"option_info":"","channel_id":8001400,"text_info":{}},"shoporders":[{"shop":{"shopid":154384940,"shop_name":"agnia colection","cb_option":false,"is_official_shop":false,"remark_type":0,"support_ereceipt":false,"seller_user_id":154386847,"shop_tag":0},"items":[{"itemid":6983580335,"modelid":134272039368,"quantity":1,"item_group_id":"5140619976724841495","insurances":[],"shopid":154384940,"shippable":true,"non_shippable_err":"","none_shippable_reason":"","none_shippable_full_reason":"","price":1920000000,"name":"TERBARU Sandal slide pria unisexs FASHION OVAL","model_name":"putih,39","add_on_deal_id":11376632,"is_add_on_sub_item":false,"is_pre_order":false,"is_streaming_price":false,"image":"sg-11134201-22110-uughjae7ghjve9","checkout":true,"categories":[{"catids":[100012,100068,100260]}],"addon_deal_sub_type":0,"is_spl_zero_interest":false,"is_prescription":false,"channel_exclusive_info":{"source_id":0,"token":""},"offerid":0,"supports_free_returns":false}],"tax_info":{"use_new_custom_tax_msg":false,"custom_tax_msg":"","custom_tax_msg_short":"","remove_custom_tax_hint":false},"tax_payable":0,"shipping_id":1,"shipping_fee_discount":0,"shipping_fee":900000000,"order_total_without_shipping":1920000000,"order_total":2820000000,"buyer_remark":"","ext_ad_info_mappings":[]}],"shipping_orders":[{"shipping_id":1,"shoporder_indexes":[0],"selected_logistic_channelid":8003,"selected_preferred_delivery_time_option_id":0,"buyer_remark":"","buyer_address_data":{"addressid":80330090,"address_type":0,"tax_address":""},"fulfillment_info":{"fulfillment_flag":64,"fulfillment_source":"","managed_by_sbs":false,"order_fulfillment_type":2,"warehouse_address_id":0,"is_from_overseas":false},"order_total":2820000000,"order_total_without_shipping":1920000000,"selected_logistic_channelid_with_warning":null,"shipping_fee":900000000,"shipping_fee_discount":0,"shipping_group_description":"","shipping_group_icon":"","tax_payable":0,"is_fsv_applied":false,"prescription_info":{"images":null,"required":false,"max_allowed_images":5}}],"fsv_selection_infos":[],"buyer_info":{"share_to_friends_info":{"display_toggle":false,"enable_toggle":false,"allow_to_share":false},"kyc_info":null,"checkout_email":""},"client_event_info":{"is_platform_voucher_changed":false,"is_fsv_changed":false},"buyer_txn_fee_info":{"title":"Biaya Penanganan","description":"Biaya Penanganan untuk transaksi ini adalah Rp0","learn_more_url":""},"disabled_checkout_info":{"description":"","auto_popup":false,"error_infos":[]},"can_checkout":true,"buyer_service_fee_info":{"learn_more_url":"https://shopee.co.id/m/biaya-layanan"},"__raw":{},"_cft":[3333739],"captcha_version":1,"device_info":{"device_sz_fingerprint":"Unw6657nP0Ycdw7Nw8G8jw==|f86Uqw/4uPyY3yyxB4iYxdQHu41r4N3DrsQTCzWa0XBA9ML0IPvXou0dlofrwKlvl+vO+ZSRgy6F3kOgqGg/yeAiGP/gdTGoNDMz|ZdTvMjKnTZ+JSHGy|06|3"}}');

$response = curl_exec($ch);

curl_close($ch);
print_r($response);
exit;
system(`
curl 'https://shopee.co.id/api/v4/checkout/place_order' \
    -H 'authority: shopee.co.id' \
    -H 'accept: application/json' \
    -H 'accept-language: en-US,en;q=0.9,es;q=0.8,id;q=0.7' \
    -H 'content-type: application/json' \
    -H 'cookie: __LOCALE__null=ID; csrftoken=mi6cJfV9O8QRgObZ3tKJ8D60rjaaNX5o; _fbp=fb.2.1661136956422.2013471656; SPC_F=Ub1MWzVp5HW20KFRmcboAY9oXJ9gNPp9; REC_T_ID=f1bbf986-21c5-11ed-9666-aae6d9218742; _QPWSDCXHZQA=53d5776c-c34d-4b14-ad53-ebb6f2ea64a4; G_ENABLED_IDPS=google; SPC_CLIENTID=VWIxTVd6VnA1SFcybulbfuwrsnihifqx; _ga_KK6LLGGZNQ=GS1.1.1661741802.1.0.1661741802.0.0.0; _tt_enable_cookie=1; _ttp=104f508c-7965-422d-8cad-5df1d7d0a951; _gcl_aw=GCL.1664290563.CjwKCAjwvsqZBhAlEiwAqAHElUoh2yklK-H6vR_DYKmV50fF02-d41oF439BL4vzQuFDZ1l1h_fw3BoCWqcQAvD_BwE; _fbc=fb.2.1665489596452.IwAR2EV5-h5OGjfWd80x9sUDQ9t-6tse_c3s2DJ0tYJAxsJKZgwSHpAP_SP0g; SPC_IA=1; _gcl_au=1.1.1680556960.1669508558; _ga_8TJ45E514C=GS1.1.1670820747.1.1.1670821049.60.0.0; __stripe_mid=415b03bf-817f-43e3-8f3e-765f29020704f2c88c; SPC_LIUP_53452969=; _ga_CGXK257VSB=GS1.1.1670848881.8.0.1670848883.58.0.0; cto_bundle=p0UiSV9xMiUyQlJuUiUyRkZSJTJGVm5BbjBZdkxQUFFubUVTNmJRQzFnd0tPeVd3RE1yWFRJU29lN2Y2SDVaSkVYdWdsS3M5ZnpPMUpNaWM3dldFeEdGU1F2RjBCd2pxUnB5blZObG9DWXh1NUVxTEVSZmdqN1ZMdWJ5TWhESk1nJTJCRFhDRnF6cE9JMDJKTUM2TkxYMXpxU3YwNWxzVW9yZyUzRCUzRA; SPC_ST=.MGV2YTk2a3lyWjh0MVNKaDo4pPby+7a/hgXmKqs/2QUGa3z7qDUIeEqAIT3d2y5rxxtPw2cOBpKH/gaz1tPIBpjUZDa42OKKAwWy9LoQZMfTsC2mMXkKsT46296QPRWGydawSi22fdu5oEaTlt1lJYUHLJtCgxvlGj0CGNtheNGMr/Qt56mU4/IVrP1KCg0dOhZWm5WFCrabtm7qqcy9GA==; SPC_U=53452969; SPC_T_ID=XWb6ziX+AUpQGLABw08bBSon2rAvPdu/zCbFMOESFzp1sFMUZZ1W2qs2So39ARZoMVzF0+QP4a7HP849sLHWS0oRky3APIuZlYN+K/imRaWZVAmUXrELa8tFDe/DZTjEVx4D2WTVMtJP+fwN1kA99tmyV1VJmMoVyMhztNv8cD4=; SPC_T_IV=c3MxTXBMSmNldkM3dGJ5Qw==; _gid=GA1.3.295752234.1671709092; SPC_T_IV="c3MxTXBMSmNldkM3dGJ5Qw=="; SPC_T_ID="XWb6ziX+AUpQGLABw08bBSon2rAvPdu/zCbFMOESFzp1sFMUZZ1W2qs2So39ARZoMVzF0+QP4a7HP849sLHWS0oRky3APIuZlYN+K/imRaWZVAmUXrELa8tFDe/DZTjEVx4D2WTVMtJP+fwN1kA99tmyV1VJmMoVyMhztNv8cD4="; SPC_R_T_IV=c3MxTXBMSmNldkM3dGJ5Qw==; SPC_SI=kteiYwAAAAAxMEkwUWNlND/VGwAAAAAAWWdoQlJabDc=; SPC_R_T_ID=XWb6ziX+AUpQGLABw08bBSon2rAvPdu/zCbFMOESFzp1sFMUZZ1W2qs2So39ARZoMVzF0+QP4a7HP849sLHWS0oRky3APIuZlYN+K/imRaWZVAmUXrELa8tFDe/DZTjEVx4D2WTVMtJP+fwN1kA99tmyV1VJmMoVyMhztNv8cD4=; AMP_TOKEN=%24NOT_FOUND; _ga=GA1.1.732921704.1661136957; shopee_webUnique_ccd=Unw6657nP0Ycdw7Nw8G8jw%3D%3D%7Cf86Uqw%2F4uPyY3yyxB4iYxdQHu41r4N3DrsQTCzWa0XBA9ML0IPvXou0dlofrwKlvl%2BvO%2BZSRgy6F3kOgqGg%2FyeAiGP%2FgdTGoNDMz%7CZdTvMjKnTZ%2BJSHGy%7C06%7C3; ds=51d9a46bb315db7b65913168f826afd4; _ga_SW6D8G0HXK=GS1.1.1672034184.68.1.1672034391.59.0.0; SPC_EC=d29tUDlvcnFGSU9yOTBmMBqmaIYrVs+trSfjSETepdRwcm1LetaPnY3RTBFBn5xM6geuhiZpUDaIUoqEkaSCaKFnwEtbMmZrRW2+n/o/chEiXfG5TATtLeNB8y4IkWtfJPXWyLEEBuBXIdqIkKXQfsGxbDrUjU1ZEImP4gbnNwQ=' \
    -H 'dnt: 1' \
    -H 'origin: https://shopee.co.id' \
    -H 'referer: https://shopee.co.id/checkout/?state=H8KLCAAAAAAAAAPChVXDm8KSw6I4DMO9wpc8w7MQCMOQwqR%2FZWvDimVsBcK8w63DmBlfGMKoKcO%2BfSRfwpLDgMO2w5TDkg%2FCnUjCsiLDqcOoHMO%2FbsKYGELDs8O5T8KHwr%2FCj8Kuw7%2FCsWnCghrDgQc%2BTsONw6fDtsO4wrFrwrs9w75tGn%2FCtcKTdRLCnMOHw6jDn8OpwrXDucOMw7%2FClcOEw5DDg8K%2BO8Otw7t9w7vDnDQqw4DCmMKjw6jCicK8w4fDvsOUHU5tw5cdNsONaCXDqHTCpMObw68of8OfHU%2FCm8OmZ8OkJsKow7BAw7vCpsOhUjJrwpgEwq5ZCsOddh%2FDh2PCt8ODw5zCnhXCp8KPZ0bDucKbw4%2FCgWsPw7nCs8Osw6JswpzDksKZw6bCsMOdwrfDh23Dn39gF8O7w5N%2BwrvDrw8NRhkfHTcCwqhCbFdcwrkxwqAZw5zChcKOXsOdwoApM8OYw5TCmsKNTkDDisOVw6JYw6wXGMOMw5pgwoM%2BTjjCi8Ogw5nDoADCmMKDEMKdw7HCpcKOw6cPwprClcKaJmUuwrnDtmQBDSLCgGQTf8KMYALCq8KfwpU8cMO6w5gNB8KrLH4Ae8K0U8OAw4dSR8OTLDVSwrpTw5tiX1QQw5xDwq3DtcKJRU3DjsKONh3CrDnCowcmwqzCmkvDmzTCqcOewrnCusKbwo3DogpubsO4L8Oew5zDv8O3TsKBYMOmGsKlw7LDvMKswrFDB8OcUyMmasK9acOOVMO5w7LCicORX3LCuAbDrgwbwq0DVmzDmMKCwoPCn1E5w4xxBsK2w7RcwoDCmsKdfhrCqzfCucKod8ONw4Ngw51Yw6sqR2g5w59Mw7gowr7DmMOaw4EQwo7CuhBlTDwGw4vDuDTDqcOHS2RFGMKHw6FvLENawoEqw6klw5zClFhWwqjCvsOKMsKjw7w6w6AEw4FNTsKZwpDDrcOBwprCi8KMwobCnTUXX2d7w4%2FDlnN8YHV1YVZIZ8OHwrzCplfCjkPDhMKCB8Knw4BIwr98XHnCnMOLA13Cl8KLwobCuTkwwoTDk8K7wpVrbX9RwpbClG7DqcO0w6shSsOCwoxnwprCn8KNwoHDgcOIwpUuwrgJw64CC8KPCcOSwqYIwq1Sw4Fyw70GwrdVE8OEw6J3w4gSwqQXwpALwp3CkV44w6ZXM34sw7DDu8KcJz3Di1LChXR2WsKYVyJywrdLVsODw4fCssKww5PDlRpgJsKOZ0I2w7PCusKeXgvDncODwojDpjPCuAh1w7DCqD8OwrzCn2lWw54zOcK7wq5tw7t2U8KNw4tUwqjDlGLCrF%2FCq8KawqDDrUXDucKgRMOdw6pCw7PDrl1IViLCjMO9ScK4Jw1rX8Okw4XDgQDCjlhCIsKLwqLCgsOwwqPCnsKzKikZwpPDvw3Dt8OaZgAzw6gYw6bChVNrWUIMR37CoQIowoQoEcO1wqDCtF7Dr8Oqwottw5AcaX7DnMK%2FRmbCjcONwoDCjMOcYEYkP8Kyw67CvMKQMcO3wrs%2BwpRnwopCw7kLV8O1aknDpsOqwrhLwoPCtDsowofDjMKSwqzCoh7DlcOtISnDjsO5w6LChMOww41EfcKuFht5wokcwq3DoMKOw6AIFcOkEsOQwo0TbMOgwrjDtcObfsOXw6bDnwrCo8OqRsO1GkjDl8OwwozCsMKROMOewrfDv8KNwq7DnlXDlsOvwpLCvRzCph1CPcKgwo3CnncKwrdgTMOAZMK2RR%2FCsG9ywrzDpE3ClwPCiXXCkjUFM8K0AsOBV3gdcUfDm1BuwpDDqsKkdcKDcsKHwqDCrkvCgHE5wplvWsKkw4TCqsORwpY4YiTDigtdw51%2Bw6XCqcKLRMKzw6XDsl8swpPCoMKcb8KJRMKscDdsAChpw4jDosOBZcOFJMOrdhnDhHzCicK%2FdsK5wpjDnyDDuFvDmMO3w74bfzNkWMOnw4nDr2bDtMKfw48%2Fw45JwrJxQQkAAA%3D%3D' \
    -H 'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "Linux"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-origin' \
    -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36' \
    -H 'x-api-source: pc' \
    -H 'x-csrftoken: mi6cJfV9O8QRgObZ3tKJ8D60rjaaNX5o' \
    -H 'x-requested-with: XMLHttpRequest' \
    -H 'x-sap-access-f: 3.2.108.4.0|13|2.5.0-2_5.5.223_0_4406|6debf478eb6e45ee9c2255bb20fb817044a984cc92844b|10900|100' \
    -H 'x-sap-access-s: V5StwDIhPSrVpJtnwL1PDw0mtgGYktuKrMs_N4zPsIs=' \
    -H 'x-sap-access-t: 1672035487' \
    -H 'x-shopee-language: id' \
    --data-raw '{"client_id":0,"cart_type":0,"timestamp":1672034343,"checkout_price_data":{"merchandise_subtotal":1920000000,"shipping_subtotal_before_discount":900000000,"shipping_discount_subtotal":0,"shipping_subtotal":900000000,"tax_payable":0,"tax_exemption":0,"custom_tax_subtotal":0,"promocode_applied":null,"credit_card_promotion":null,"shopee_coins_redeemed":null,"group_buy_discount":0,"bundle_deals_discount":null,"price_adjustment":null,"buyer_txn_fee":0,"buyer_service_fee":100000000,"insurance_subtotal":0,"insurance_before_discount_subtotal":0,"insurance_discount_subtotal":0,"vat_subtotal":0,"total_payable":2920000000},"order_update_info":{},"dropshipping_info":{"enabled":false,"name":"","phone_number":""},"promotion_data":{"can_use_coins":true,"use_coins":false,"platform_vouchers":[],"free_shipping_voucher_info":{"free_shipping_voucher_id":0,"free_shipping_voucher_code":"","disabled_reason":null,"banner_info":{"msg":"","learn_more_msg":""},"required_be_channel_ids":[],"required_spm_channels":[]},"highlighted_platform_voucher_type":-1,"shop_voucher_entrances":[{"shopid":154384940,"status":true}],"applied_voucher_code":null,"voucher_code":null,"voucher_info":{"coin_earned":0,"voucher_code":null,"coin_percentage":0,"discount_percentage":0,"discount_value":0,"promotionid":0,"reward_type":0,"used_price":0},"invalid_message":"","price_discount":0,"coin_info":{"coin_offset":1000000,"coin_used":10,"coin_earn_by_voucher":0,"coin_earn":0},"card_promotion_id":null,"card_promotion_enabled":false,"promotion_msg":""},"selected_payment_channel_data":{"version":2,"option_info":"","channel_id":8001400,"text_info":{}},"shoporders":[{"shop":{"shopid":154384940,"shop_name":"agnia colection","cb_option":false,"is_official_shop":false,"remark_type":0,"support_ereceipt":false,"seller_user_id":154386847,"shop_tag":0},"items":[{"itemid":6983580335,"modelid":134272039368,"quantity":1,"item_group_id":"5140619976724841495","insurances":[],"shopid":154384940,"shippable":true,"non_shippable_err":"","none_shippable_reason":"","none_shippable_full_reason":"","price":1920000000,"name":"TERBARU Sandal slide pria unisexs FASHION OVAL","model_name":"putih,39","add_on_deal_id":11376632,"is_add_on_sub_item":false,"is_pre_order":false,"is_streaming_price":false,"image":"sg-11134201-22110-uughjae7ghjve9","checkout":true,"categories":[{"catids":[100012,100068,100260]}],"addon_deal_sub_type":0,"is_spl_zero_interest":false,"is_prescription":false,"channel_exclusive_info":{"source_id":0,"token":""},"offerid":0,"supports_free_returns":false}],"tax_info":{"use_new_custom_tax_msg":false,"custom_tax_msg":"","custom_tax_msg_short":"","remove_custom_tax_hint":false},"tax_payable":0,"shipping_id":1,"shipping_fee_discount":0,"shipping_fee":900000000,"order_total_without_shipping":1920000000,"order_total":2820000000,"buyer_remark":"","ext_ad_info_mappings":[]}],"shipping_orders":[{"shipping_id":1,"shoporder_indexes":[0],"selected_logistic_channelid":8003,"selected_preferred_delivery_time_option_id":0,"buyer_remark":"","buyer_address_data":{"addressid":80330090,"address_type":0,"tax_address":""},"fulfillment_info":{"fulfillment_flag":64,"fulfillment_source":"","managed_by_sbs":false,"order_fulfillment_type":2,"warehouse_address_id":0,"is_from_overseas":false},"order_total":2820000000,"order_total_without_shipping":1920000000,"selected_logistic_channelid_with_warning":null,"shipping_fee":900000000,"shipping_fee_discount":0,"shipping_group_description":"","shipping_group_icon":"","tax_payable":0,"is_fsv_applied":false,"prescription_info":{"images":null,"required":false,"max_allowed_images":5}}],"fsv_selection_infos":[],"buyer_info":{"share_to_friends_info":{"display_toggle":false,"enable_toggle":false,"allow_to_share":false},"kyc_info":null,"checkout_email":""},"client_event_info":{"is_platform_voucher_changed":false,"is_fsv_changed":false},"buyer_txn_fee_info":{"title":"Biaya Penanganan","description":"Biaya Penanganan untuk transaksi ini adalah Rp0","learn_more_url":""},"disabled_checkout_info":{"description":"","auto_popup":false,"error_infos":[]},"can_checkout":true,"buyer_service_fee_info":{"learn_more_url":"https://shopee.co.id/m/biaya-layanan"},"__raw":{},"_cft":[3333739],"captcha_version":1,"device_info":{"device_sz_fingerprint":"Unw6657nP0Ycdw7Nw8G8jw==|f86Uqw/4uPyY3yyxB4iYxdQHu41r4N3DrsQTCzWa0XBA9ML0IPvXou0dlofrwKlvl+vO+ZSRgy6F3kOgqGg/yeAiGP/gdTGoNDMz|ZdTvMjKnTZ+JSHGy|06|3"}}' \
    --compressed
`);
exit;

system(`
curl 'https://shopee.co.id/api/v4/account/address/get_user_address_list?with_warehouse_whitelist_status=true' \
    -H 'authority: shopee.co.id' \
    -H 'accept: */*' \
    -H 'accept-language: en-US,en;q=0.9,es;q=0.8,id;q=0.7' \
    -H 'cookie: __LOCALE__null=ID; csrftoken=mi6cJfV9O8QRgObZ3tKJ8D60rjaaNX5o; _fbp=fb.2.1661136956422.2013471656; SPC_F=Ub1MWzVp5HW20KFRmcboAY9oXJ9gNPp9; REC_T_ID=f1bbf986-21c5-11ed-9666-aae6d9218742; _QPWSDCXHZQA=53d5776c-c34d-4b14-ad53-ebb6f2ea64a4; G_ENABLED_IDPS=google; SPC_CLIENTID=VWIxTVd6VnA1SFcybulbfuwrsnihifqx; _ga_KK6LLGGZNQ=GS1.1.1661741802.1.0.1661741802.0.0.0; _tt_enable_cookie=1; _ttp=104f508c-7965-422d-8cad-5df1d7d0a951; _gcl_aw=GCL.1664290563.CjwKCAjwvsqZBhAlEiwAqAHElUoh2yklK-H6vR_DYKmV50fF02-d41oF439BL4vzQuFDZ1l1h_fw3BoCWqcQAvD_BwE; _fbc=fb.2.1665489596452.IwAR2EV5-h5OGjfWd80x9sUDQ9t-6tse_c3s2DJ0tYJAxsJKZgwSHpAP_SP0g; SPC_IA=1; _gcl_au=1.1.1680556960.1669508558; _ga_8TJ45E514C=GS1.1.1670820747.1.1.1670821049.60.0.0; __stripe_mid=415b03bf-817f-43e3-8f3e-765f29020704f2c88c; SPC_LIUP_53452969=; _ga_CGXK257VSB=GS1.1.1670848881.8.0.1670848883.58.0.0; cto_bundle=p0UiSV9xMiUyQlJuUiUyRkZSJTJGVm5BbjBZdkxQUFFubUVTNmJRQzFnd0tPeVd3RE1yWFRJU29lN2Y2SDVaSkVYdWdsS3M5ZnpPMUpNaWM3dldFeEdGU1F2RjBCd2pxUnB5blZObG9DWXh1NUVxTEVSZmdqN1ZMdWJ5TWhESk1nJTJCRFhDRnF6cE9JMDJKTUM2TkxYMXpxU3YwNWxzVW9yZyUzRCUzRA; SPC_ST=.MGV2YTk2a3lyWjh0MVNKaDo4pPby+7a/hgXmKqs/2QUGa3z7qDUIeEqAIT3d2y5rxxtPw2cOBpKH/gaz1tPIBpjUZDa42OKKAwWy9LoQZMfTsC2mMXkKsT46296QPRWGydawSi22fdu5oEaTlt1lJYUHLJtCgxvlGj0CGNtheNGMr/Qt56mU4/IVrP1KCg0dOhZWm5WFCrabtm7qqcy9GA==; SPC_U=53452969; SPC_T_ID=XWb6ziX+AUpQGLABw08bBSon2rAvPdu/zCbFMOESFzp1sFMUZZ1W2qs2So39ARZoMVzF0+QP4a7HP849sLHWS0oRky3APIuZlYN+K/imRaWZVAmUXrELa8tFDe/DZTjEVx4D2WTVMtJP+fwN1kA99tmyV1VJmMoVyMhztNv8cD4=; SPC_T_IV=c3MxTXBMSmNldkM3dGJ5Qw==; _gid=GA1.3.295752234.1671709092; SPC_T_IV="c3MxTXBMSmNldkM3dGJ5Qw=="; SPC_T_ID="XWb6ziX+AUpQGLABw08bBSon2rAvPdu/zCbFMOESFzp1sFMUZZ1W2qs2So39ARZoMVzF0+QP4a7HP849sLHWS0oRky3APIuZlYN+K/imRaWZVAmUXrELa8tFDe/DZTjEVx4D2WTVMtJP+fwN1kA99tmyV1VJmMoVyMhztNv8cD4="; SPC_R_T_IV=c3MxTXBMSmNldkM3dGJ5Qw==; SPC_SI=kteiYwAAAAAxMEkwUWNlND/VGwAAAAAAWWdoQlJabDc=; SPC_R_T_ID=XWb6ziX+AUpQGLABw08bBSon2rAvPdu/zCbFMOESFzp1sFMUZZ1W2qs2So39ARZoMVzF0+QP4a7HP849sLHWS0oRky3APIuZlYN+K/imRaWZVAmUXrELa8tFDe/DZTjEVx4D2WTVMtJP+fwN1kA99tmyV1VJmMoVyMhztNv8cD4=; AMP_TOKEN=%24NOT_FOUND; _ga=GA1.1.732921704.1661136957; ds=e6c1ce60085cf12705a3de447b7fb843; _dc_gtm_UA-61904553-8=1; SPC_EC=ZGpEWGwzcTQ3RTFYY254TNK1spXD/e5qGAltljjKbZOA3MSTXXeJCvXTbNQhGo23+rkmzpBKj8lRISS1m9znaQcjaNE0yBaSQjo89VXIgP/S0hf58PQaWLAgqkvQowlWueH5EZ8oCzIlrMM0VkzBReCkeqgm6YYiqlDuj6t5buE=; shopee_webUnique_ccd=f18X1rXNZ%2BoRnDkopirsMg%3D%3D%7Cf82Uqw%2F4uPyY3yyxB4iYxdQHu41r4N3DrsQTCzWa0XBA9ML0IPvXou0dlofrwKlvl%2BvO%2BZSRgy6F3kOgqGg%2BzOEvFP7rdTGoNDMz%7CZdTvMjKnTZ%2BJSHGy%7C06%7C3; _ga_SW6D8G0HXK=GS1.1.1672019527.66.1.1672021390.55.0.0' \
    -H 'dnt: 1' \
    -H 'if-none-match-: 55b03-7404008ec63e78568f7cc4f6f8a5ecc9' \
    -H 'referer: https://shopee.co.id/user/account/address' \
    -H 'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "Linux"' \
    -H 'sec-fetch-dest: empty' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-site: same-origin' \
    -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36' \
    -H 'x-api-source: pc' \
    -H 'x-requested-with: XMLHttpRequest' \
    -H 'x-shopee-language: id' \
    --compressed
`);
exit;


system(`
curl 'https://shopee.co.id/api/v4/checkout/place_order' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0' -H 'Accept: application/json' --compressed -H 'Content-Type: application/json' -H 'Cookie: SPC_R_T_ID=xJ+VACKwQws5QKSV95UvCLNxdatccVvscwNllUebElbEmm/9wo85kt8LSWswlGIZ/E6rzvybEdsOTjLLbQx1NkMdPDmzxITEvzb8jRZ+O4Y=; SPC_IA=-1; SPC_EC=MUJvZ3JldzUzaTZZbGhUUTFVZAAPySZOv8Yk8LNfPQW4Vw1OlCnSW/i0LF6htslfF7ms4hQ49LRIVpk+fFkDXzz70cCyFTr93HTQtujdEtJSKFNsGInMPC9LFld4ILoJsaA54hbNfevzqCvr4ulN9PWYtXhaBj3GnSv5b1CA41s=;' --data-raw '
{
    "status": 200,
    "headers": {
    
    },
    "client_id": 0,
    "cart_type": 0,
    "timestamp": 1639075873,
    "checkout_price_data": {
    "merchandise_subtotal": 264950000000,
    "total_payable": 266950000000,
    "shipping_subtotal": 1900000000,
    "shipping_subtotal_before_discount": 1900000000,
    "shipping_discount_subtotal": 0,
    "tax_payable": 0,
    "tax_exemption": 0,
    "custom_tax_subtotal": 0,
    "promocode_applied": null,
    "credit_card_promotion": null,
    "shopee_coins_redeemed": null,
    "group_buy_discount": 0,
    "bundle_deals_discount": null,
    "buyer_txn_fee": 100000000,
    "insurance_subtotal": 0,
    "insurance_before_discount_subtotal": 0,
    "insurance_discount_subtotal": 0,
    "vat_subtotal": 0
    },
    "order_update_info": {
    
    },
    "dropshipping_info": {
    "enabled": false,
    "name": "",
    "phone_number": ""
    },
    "promotion_data": {
    "applied_voucher_code": null,
    "voucher_code": null,
    "can_use_coins": false,
    "use_coins": false,
    "platform_vouchers": [
        
    ],
    "free_shipping_voucher_info": {
        "free_shipping_voucher_id": 0,
        "free_shipping_voucher_code": "",
        "disabled_reason": null,
        "banner_info": {
        "msg": "",
        "learn_more_msg": ""
        }
    },
    "invalid_message": "",
    "price_discount": 0,
    "voucher_info": {
        "coin_earned": 0,
        "voucher_code": null,
        "coin_percentage": 0,
        "discount_percentage": 0,
        "discount_value": 0,
        "promotionid": 0,
        "reward_type": 0,
        "used_price": 0
    },
    "coin_info": {
        "coin_offset": 0,
        "coin_used": 0,
        "coin_earn_by_voucher": 0,
        "coin_earn": 0
    },
    "card_promotion_id": null,
    "card_promotion_enabled": false,
    "promotion_msg": "",
    "shop_voucher_entrances": [
        {
        "shopid": 52635036,
        "status": true
        }
    ]
    },
    "selected_payment_channel_data": {
    "version": 2,
    "option_info": "",
    "channel_id": 8005200,
    "channel_item_option_info": {
        "option_info": "89052001"
    },
    "text_info": {
        
    }
    },
    "shoporders": [
    {
        "shop": {
        "shopid": 52635036,
        "shop_name": "Samsung Official Shop",
        "cb_option": false,
        "is_official_shop": true,
        "remark_type": 0,
        "support_ereceipt": false,
        "seller_user_id": 52636424,
        "shop_tag": 1
        },
        "items": [
        {
            "itemid": 10148601994,
            "modelid": 120417099479,
            "quantity": 1,
            "item_group_id": "4083998758350111097",
            "insurances": [
            {
                "insurance_product_id": "1385277174484245645",
                "name": "Proteksi Gadget",
                "description": "Melindungi gadgetmu dari kerusakan yang tidak disengaja, kerusakan akibat cairan, dan perampokan.",
                "product_detail_page_url": "https://insurance.shopee.co.id/insurance/profile?from=mp_checkout&product_id=1385277174484245645",
                "premium": 4000000000,
                "premium_before_discount": 4000000000,
                "insurance_quantity": 1,
                "selected": false
            }
            ],
            "shopid": 52635036,
            "shippable": true,
            "non_shippable_err": "",
            "none_shippable_reason": "",
            "none_shippable_full_reason": "",
            "price": 264950000000,
            "name": "Samsung Galaxy M22 6/128 GB - White",
            "model_name": "White",
            "add_on_deal_id": 6027306,
            "is_add_on_sub_item": false,
            "is_pre_order": false,
            "is_streaming_price": false,
            "image": "1d79ab8ae20c0b722fde0ba122d78e17",
            "checkout": true,
            "categories": [
            {
                "catids": [
                100013,
                100073
                ]
            }
            ],
            "addon_deal_sub_type": 0
        }
        ],
        "tax_info": {
        "use_new_custom_tax_msg": false,
        "custom_tax_msg": "",
        "custom_tax_msg_short": "",
        "remove_custom_tax_hint": false
        },
        "tax_payable": 0,
        "shipping_id": 1,
        "shipping_fee_discount": 0,
        "shipping_fee": 1900000000,
        "order_total_without_shipping": 264950000000,
        "order_total": 266850000000,
        "buyer_remark": "",
        "buyer_ic_number": "",
        "ext_ad_info_mappings": [
        
        ]
    }
    ],
    "shipping_orders": [
    {
        "shipping_id": 1,
        "shoporder_indexes": [
        0
        ],
        "selected_logistic_channelid": 8003,
        "selected_preferred_delivery_time_option_id": 0,
        "buyer_remark": "",
        "buyer_address_data": {
        "addressid": 89385438,
        "address_type": 0,
        "tax_address": ""
        },
        "fulfillment_info": {
        "fulfillment_flag": 18,
        "fulfillment_source": "IDE",
        "managed_by_sbs": false,
        "order_fulfillment_type": 1,
        "warehouse_address_id": 38200003,
        "is_from_overseas": false
        },
        "order_total": 266850000000,
        "order_total_without_shipping": 264950000000,
        "selected_logistic_channelid_with_warning": null,
        "shipping_fee": 1900000000,
        "shipping_fee_discount": 0,
        "shipping_group_description": "Dikelola Shopee | Dikemas dan Dikirim Shopee",
        "shipping_group_icon": "https://cf.shopee.co.id/file/bcb4e84dd6d7003f5cfa154abc2fb34f",
        "tax_payable": 0,
        "is_fsv_applied": false,
        "buyer_ic_number": ""
    }
    ],
    "fsv_selection_infos": [
    
    ],
    "buyer_info": {
    "share_to_friends_info": {
        "display_toggle": false,
        "enable_toggle": false,
        "allow_to_share": false
    },
    "checkout_email": ""
    },
    "buyer_txn_fee_info": {
    "title": "Biaya Penanganan",
    "description": "Besar biaya penanganan adalah Rp1.000 dari total transaksi.",
    "learn_more_url": "https://shopee.co.id/events3/code/634289435/"
    },
    "disabled_checkout_info": {
    "description": "",
    "auto_popup": false,
    "error_infos": [
        
    ]
    },
    "can_checkout": true,
    "_cft": [
    11
    ],
    "device_info": {
    "device_sz_fingerprint": "WUbrHJMXvyfymqElSreR2A==|LX2cmpypeG/2F02S/vAiaueDbl4TyqWFVcVb4/pj8rB26Xr83/6uYF+sOH0UnBb1GKFRpVgSHMVPxvG0B/n4|6bbqA7+YbTUbFNfd|03|3"
    },
    "captcha_version": 1
}
'
`);
exit;

system(`
curl 'https://shopee.co.id/api/v4/checkout/get' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0' -H 'Accept: application/json' --compressed -H 'Cookie: SPC_EC=MUJvZ3JldzUzaTZZbGhUUTFVZAAPySZOv8Yk8LNfPQW4Vw1OlCnSW/i0LF6htslfF7ms4hQ49LRIVpk+fFkDXzz70cCyFTr93HTQtujdEtJSKFNsGInMPC9LFld4ILoJsaA54hbNfevzqCvr4ulN9PWYtXhaBj3GnSv5b1CA41s=' --data-raw '
{
    "shoporders":[{
        "shop":{
            "shopid":52635036
        },
        "items":[
            {
                "itemid":10148601994,
                "modelid":120417099479,
                "quantity":1,
                "item_group_id":"4083998758350111097"
            }
        ],
        "shipping_id":1
    }],
    "selected_payment_channel_data":{
        "channel_id":8005200,
        "channel_item_option_info":{
            "option_info":"89052001"
        },
        "version":2
    },
    "shipping_orders":[{
        "sync":true,
        "buyer_address_data":{
            "addressid":89376299,
            "address_type":0,
            "tax_address":""
        },
        "selected_logistic_channelid":8003,
        "shipping_id":1,
        "shoporder_indexes":[0],
        "buyer_ic_number":"",
        "selected_preferred_delivery_time_option_id":0,
        "selected_preferred_delivery_time_slot_id":null
    }]
}
'
`);
exit;
// {
//     "client_id": 0,
//     "cart_type": 0,
//     "timestamp": 1672034343,
//     "checkout_price_data": {
//         "merchandise_subtotal": 1920000000,
//         "shipping_subtotal_before_discount": 900000000,
//         "shipping_discount_subtotal": 0,
//         "shipping_subtotal": 900000000,
//         "tax_payable": 0,
//         "tax_exemption": 0,
//         "custom_tax_subtotal": 0,
//         "promocode_applied": null,
//         "credit_card_promotion": null,
//         "shopee_coins_redeemed": null,
//         "group_buy_discount": 0,
//         "bundle_deals_discount": null,
//         "price_adjustment": null,
//         "buyer_txn_fee": 0,
//         "buyer_service_fee": 100000000,
//         "insurance_subtotal": 0,
//         "insurance_before_discount_subtotal": 0,
//         "insurance_discount_subtotal": 0,
//         "vat_subtotal": 0,
//         "total_payable": 2920000000
//     },
//     "order_update_info": {},
//     "dropshipping_info": {
//         "enabled": false,
//         "name": "",
//         "phone_number": ""
//     },
//     "promotion_data": {
//         "can_use_coins": true,
//         "use_coins": false,
//         "platform_vouchers": [],
//         "free_shipping_voucher_info": {
//             "free_shipping_voucher_id": 0,
//             "free_shipping_voucher_code": "",
//             "disabled_reason": null,
//             "banner_info": {
//                 "msg": "",
//                 "learn_more_msg": ""
//             },
//             "required_be_channel_ids": [],
//             "required_spm_channels": []
//         },
//         "highlighted_platform_voucher_type": -1,
//         "shop_voucher_entrances": [
//             {
//                 "shopid": 154384940,
//                 "status": true
//             }
//         ],
//         "applied_voucher_code": null,
//         "voucher_code": null,
//         "voucher_info": {
//             "coin_earned": 0,
//             "voucher_code": null,
//             "coin_percentage": 0,
//             "discount_percentage": 0,
//             "discount_value": 0,
//             "promotionid": 0,
//             "reward_type": 0,
//             "used_price": 0
//         },
//         "invalid_message": "",
//         "price_discount": 0,
//         "coin_info": {
//             "coin_offset": 1000000,
//             "coin_used": 10,
//             "coin_earn_by_voucher": 0,
//             "coin_earn": 0
//         },
//         "card_promotion_id": null,
//         "card_promotion_enabled": false,
//         "promotion_msg": ""
//     },
//     "selected_payment_channel_data": {
//         "version": 2,
//         "option_info": "",
//         "channel_id": 8001400,
//         "text_info": {}
//     },
//     "shoporders": [
//         {
//             "shop": {
//                 "shopid": 154384940,
//                 "shop_name": "agnia colection",
//                 "cb_option": false,
//                 "is_official_shop": false,
//                 "remark_type": 0,
//                 "support_ereceipt": false,
//                 "seller_user_id": 154386847,
//                 "shop_tag": 0
//             },
//             "items": [
//                 {
//                     "itemid": 6983580335,
//                     "modelid": 134272039368,
//                     "quantity": 1,
//                     "item_group_id": "5140619976724841495",
//                     "insurances": [],
//                     "shopid": 154384940,
//                     "shippable": true,
//                     "non_shippable_err": "",
//                     "none_shippable_reason": "",
//                     "none_shippable_full_reason": "",
//                     "price": 1920000000,
//                     "name": "TERBARU Sandal slide pria unisexs FASHION OVAL",
//                     "model_name": "putih,39",
//                     "add_on_deal_id": 11376632,
//                     "is_add_on_sub_item": false,
//                     "is_pre_order": false,
//                     "is_streaming_price": false,
//                     "image": "sg-11134201-22110-uughjae7ghjve9",
//                     "checkout": true,
//                     "categories": [
//                         {
//                             "catids": [
//                                 100012,
//                                 100068,
//                                 100260
//                             ]
//                         }
//                     ],
//                     "addon_deal_sub_type": 0,
//                     "is_spl_zero_interest": false,
//                     "is_prescription": false,
//                     "channel_exclusive_info": {
//                         "source_id": 0,
//                         "token": ""
//                     },
//                     "offerid": 0,
//                     "supports_free_returns": false
//                 }
//             ],
//             "tax_info": {
//                 "use_new_custom_tax_msg": false,
//                 "custom_tax_msg": "",
//                 "custom_tax_msg_short": "",
//                 "remove_custom_tax_hint": false
//             },
//             "tax_payable": 0,
//             "shipping_id": 1,
//             "shipping_fee_discount": 0,
//             "shipping_fee": 900000000,
//             "order_total_without_shipping": 1920000000,
//             "order_total": 2820000000,
//             "buyer_remark": "",
//             "ext_ad_info_mappings": []
//         }
//     ],
//     "shipping_orders": [
//         {
//             "shipping_id": 1,
//             "shoporder_indexes": [
//                 0
//             ],
//             "selected_logistic_channelid": 8003,
//             "selected_preferred_delivery_time_option_id": 0,
//             "buyer_remark": "",
//             "buyer_address_data": {
//                 "addressid": 80330090,
//                 "address_type": 0,
//                 "tax_address": ""
//             },
//             "fulfillment_info": {
//                 "fulfillment_flag": 64,
//                 "fulfillment_source": "",
//                 "managed_by_sbs": false,
//                 "order_fulfillment_type": 2,
//                 "warehouse_address_id": 0,
//                 "is_from_overseas": false
//             },
//             "order_total": 2820000000,
//             "order_total_without_shipping": 1920000000,
//             "selected_logistic_channelid_with_warning": null,
//             "shipping_fee": 900000000,
//             "shipping_fee_discount": 0,
//             "shipping_group_description": "",
//             "shipping_group_icon": "",
//             "tax_payable": 0,
//             "is_fsv_applied": false,
//             "prescription_info": {
//                 "images": null,
//                 "required": false,
//                 "max_allowed_images": 5
//             }
//         }
//     ],
//     "fsv_selection_infos": [],
//     "buyer_info": {
//         "share_to_friends_info": {
//             "display_toggle": false,
//             "enable_toggle": false,
//             "allow_to_share": false
//         },
//         "kyc_info": null,
//         "checkout_email": ""
//     },
//     "client_event_info": {
//         "is_platform_voucher_changed": false,
//         "is_fsv_changed": false
//     },
//     "buyer_txn_fee_info": {
//         "title": "Biaya Penanganan",
//         "description": "Biaya Penanganan untuk transaksi ini adalah Rp0",
//         "learn_more_url": ""
//     },
//     "disabled_checkout_info": {
//         "description": "",
//         "auto_popup": false,
//         "error_infos": []
//     },
//     "can_checkout": true,
//     "buyer_service_fee_info": {
//         "learn_more_url": "https://shopee.co.id/m/biaya-layanan"
//     },
//     "__raw": {},
//     "_cft": [
//         3333739
//     ],
//     "captcha_version": 1,
//     "device_info": {
//         "device_sz_fingerprint": "Unw6657nP0Ycdw7Nw8G8jw==|f86Uqw/4uPyY3yyxB4iYxdQHu41r4N3DrsQTCzWa0XBA9ML0IPvXou0dlofrwKlvl+vO+ZSRgy6F3kOgqGg/yeAiGP/gdTGoNDMz|ZdTvMjKnTZ+JSHGy|06|3"
//     }
// }