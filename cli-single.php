<?php

if (false) {
    print_r("\n");
    print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - PRODUCT");
    print_r("\n    => ");
    $res = curl("GET", "https://mall.shopee.co.id/api/v2/item/get?shopid=" . $shopId . "&itemid=" . $data["products"][$shopId]["item_id"]);
    if ($res["status"] && @$res["result"]["item"]) {
        $item = $res["result"]["item"];
        if ($data["flashsale_time"]) {
            $flashsaleTimeTemp = strtotime(date($data["flashsale_time"]));
        } else {
            if (@$item["upcoming_flash_sale"]) {
                $flashsaleTimeTemp = $item["upcoming_flash_sale"]["start_time"];
            } else if (@$item["flash_sale"]) {
                $flashsaleTimeTemp = $item["flash_sale"]["start_time"];
            } else {
                $flashsaleTimeTemp = strtotime(date('Y-m-d H:i:s'));
            }
        }

        $flashsaleTime = $flashsaleTimeTemp;
        print_r($item["name"]);
    } else {
        json(false, "Product not found.");
        print_r("\n\n");
        exit;
    }
} else {
    $flashsaleTime = strtotime(date($data["flashsale_time"]));
}

print_r("\n");
print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - LOGISTIC");
print_r("\n    => ");
$url = "https://mall.shopee.co.id/api/v4/pdp/get_shipping_info?shopid=" . $shopId . "&itemid=" . $data["products"][$shopId]["item_id"] . "&state=" . urlencode($address["state"]) . "&city=" . urlencode($address["city"]) . "&district=" . urlencode($address["district"]) . "&town=";
// print_r($url);
// exit;
$res = curl("GET", $url);
if ($res["status"] && @$res["result"]["data"]["shipping_infos"]) {
    $tempShippingCheapest = 133333333333337;
    $shippings = [];
    foreach ($res["result"]["data"]["shipping_infos"] as $tempShippingId => $tempShipping) {
        if (!$tempShipping["warning_msg"] || $tempShipping["warning_msg"] == 'Cek Ongkir di halaman Checkout') {
            if ($product["logistic"]["id"]) {
                if ($tempShipping["channel"]["channelid"] === $product["logistic"]["id"]) {
                    $shippings[$shopId] = $tempShipping;
                }
            } else {
                if ($tempShipping["original_cost"] <= $tempShippingCheapest) {
                    $tempShippingCheapest = $tempShipping["original_cost"];
                    $shippings[$shopId] = $tempShipping;
                }
            }
        }
    }

    if (count($shippings) > 0 && @array_key_exists($shopId, $shippings)) {
        $shippingFee = $shippings[$shopId]["original_cost"];

        print_r($shippings[$shopId]["channel"]["channelid"] . ' ' . (strlen($shippings[$shopId]["channel"]["channelid"]) === 4 ? ' ' : '') . ': - ' . $shippings[$shopId]["channel"]["name"]);
        print_r("\n               - " . date("Y-m-d", $shippings[$shopId]["delivery_info"]["estimated_delivery_date_from"]) . " -> " . date("Y-m-d", $shippings[$shopId]["delivery_info"]["estimated_delivery_date_to"]));
        print_r("\n               - " . handleRupiahFormat($shippingFee));
    } else {
        print_r("The shipping option is not found.");
        print_r("\n\n");
        exit;
    }
} else {
    json(false, "Logistic service type is not found.");
    print_r("\n\n");
    exit;
}

print_r("\n");
print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - ADD TO CHART");
print_r("\n    => ");
$res = curl("POST", "https://mall.shopee.co.id/api/v4/cart/add_to_cart", [
    0               => 'content-type: application/json',
    'accept'        => 'application/json',
    'x-csrftoken'   => $settings["csrf"],
],
[], [
    "quantity" => 1,
    "donot_add_quantity" => true,
    "checkout"  => true,
    "client_source" => 1,
    "update_checkout_only"  => false,

    "shopid" => $shopId,
    "itemid" => $product["item_id"],
    "modelid" => $product["model_id"],
]);

if (@$res["result"]["error"] === 0) {
    $cartItem = $res["result"]["data"]["cart_item"];
    $data["products"][$shopId]["item_id"] = $cartItem["itemid"];
    $data["products"][$shopId]["model_id"] = $cartItem["modelid"];
    $data["products"][$shopId]["item_group_id"] = $cartItem["item_group_id"];
    $data["products"][$shopId]["price"] = $cartItem["price"];

    print_r("SUCCESS : " . $shopId . " - " . $data["products"][$shopId]["item_id"] . " - " . $data["products"][$shopId]["model_id"] . " - " . handleRupiahFormat($res["result"]["data"]["cart_item"]["price"]));

    do {
        print_r("\n");
        print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - ACCOUNT INFO");
        print_r("\n    => ");
        $res = curl(
            "POST",
            "https://mall.shopee.co.id/api/v4/checkout/get",
            [
                'content-type'      => 'application/json',
                'accept'            => 'application/json',
                'x-csrftoken'       => $settings["csrf"],
                'x-sap-access-f'    => '3.2.108.4.0|13|2.5.0-2_5.4.223_0_2374|24e9df3b22a54764b2162f5d4e66ec669f1f587073ab42|10900|100',
                'x-sap-access-s'    => '_Qsy31V-rBQUrJ8AeQZcWla4L-kDTzQhVyNVM5e1Joc=',
                'x-sap-access-t'    => '1672034344',
            ],
            [],
            '
            {
                "shoporders":[{
                    "shop":{
                        "shopid":' . $shopId . '
                    },
                    "items":[
                        {
                            "itemid":' . $data["products"][$shopId]["item_id"] . ',
                            "modelid":' . $data["products"][$shopId]["model_id"] . ',
                            "quantity":1,
                            "item_group_id":"' . $data["products"][$shopId]["item_group_id"] . '"
                        }
                    ],
                    "shipping_id":1
                }],
                "selected_payment_channel_data":{
                    "channel_id":' . $data["payment"]["channel"]["id"] . ',
                    "channel_item_option_info":{
                        "option_info":"' . $data["payment"]["channel"]["option_info"] . '"
                    },
                    "version":2
                },
                "shipping_orders":[{
                    "sync":true,
                    "buyer_address_data":{
                        "addressid":' . $data["shipping"]["address_id"] . '
                    },
                    "selected_logistic_channelid":' . $shippings[$shopId]["channel"]["channelid"] . ',
                    "shipping_id":1,
                    "shoporder_indexes":[0],
                    "selected_preferred_delivery_time_option_id":' . $data["products"][$shopId]["logistic"]["delivery_time_option_id"] . '
                }]
            }
            ',
            "ARRAY",
            "JSON"
        );

        if (@$res["result"]["checkout_price_data"]) {
            $item = $res["result"];
            $shopeePay = handleRupiahFormat($res["result"]["payment_channel_info"]["channels"][0]["balance"]);
            $timesTamp = $res["result"]["timestamp"];

            print_r("Checkout    : " . ($item['can_checkout'] ? 'true' : 'false'));
            print_r("\n       Payment     : " . $data["payment"]["channel"]["id"] . ($data["payment"]["channel"]["option_info"] ? " - " . $data["payment"]["channel"]["option_info"] : ''));
            print_r("\n       Timestamp   : " . date("Y-m-d H:i:s", $item["timestamp"]) . " - " . $item["timestamp"]);
            print_r("\n       FS Time     : " . date("Y-m-d H:i:s", $flashsaleTime) . " - " . $flashsaleTime);
            print_r("\n       ShopeePay   : " . $shopeePay);
            print_r("\n       Item        : " . handleRupiahFormat($item["checkout_price_data"]["merchandise_subtotal"]));
            print_r("\n       Ongkir      : " . handleRupiahFormat($item['checkout_price_data']['shipping_subtotal']));
            print_r("\n       Fee         : " . handleRupiahFormat($item['checkout_price_data']['buyer_service_fee']));
            print_r("\n       Total       : " . handleRupiahFormat($item['checkout_price_data']['total_payable']));

            $isWhile = false;
        } else {
            json($res["status"], $res["result"]);

            ++$j;
            $isWhile = true;
        }
    } while ($isWhile);

    $canBeAddedToCart = true;
} else {
    print_r("FAILED      : Cannot add product to cart");

    print_r("\n       Payment     : " . $data["payment"]["channel"]["id"] . ($data["payment"]["channel"]["option_info"] ? " - " . $data["payment"]["channel"]["option_info"] : ''));
    print_r("\n       Time        : " . date("Y-m-d H:i:s", $flashsaleTime) . " - " . $flashsaleTime);
    print_r("\n       Fee         : " . handleRupiahFormat($buyerFee));

    $canBeAddedToCart = false;
}

$servers = [
    "128.199.91.195",
];
$res = curl("GET", 'https://api.ipify.org/?format=json');
if (array_key_exists('ip', $res['result'])) {
    $isServer = in_array($res['result']['ip'], $servers);
} else {
    $isServer = false;
}

print_r("\n\n");

do {
    date_default_timezone_set("Asia/Jakarta");

    if (!$isServer) {
        print_r(date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) .  " - "  . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - WAITING\r");
    }
} while (date("Y-m-d H:i:s") < date("Y-m-d H:i:s", $flashsaleTime));
if ($isServer) {
    print_r(date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) .  " - "  . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - WAITING\r");
}
print_r("\n    => SUCCESS");

print_r("\n\n");
do {
    print_r(date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - REFRESH CHART\r");
    $res = curl("POST", "https://mall.shopee.co.id/api/v4/cart/add_to_cart", [
        0               => 'content-type: application/json',
        'accept'        => 'application/json',
        'x-csrftoken'   => $settings["csrf"],
    ], [
    ], [
        "quantity" => 1,
        "donot_add_quantity" => true,

        "shopid" => $shopId,
        "itemid" => $product["item_id"],
        "modelid" => $product["model_id"],
    ]);
    if (@$res["result"]["error"] === 0) {
        print_r("    => SUCCESS: " . $shopId . " - " . $product["item_id"] . " - " . $product["model_id"] . " - " . handleRupiahFormat($res["result"]["data"]["cart_item"]["price"]));

        $cartItem = $res["result"]["data"]["cart_item"];
        $data["products"][$shopId]["item_id"] = $cartItem["itemid"];
        $data["products"][$shopId]["model_id"] = $cartItem["modelid"];
        $data["products"][$shopId]["item_group_id"] = $cartItem["item_group_id"];
        $data["products"][$shopId]["price"] = $cartItem["price"];

        do {
            print_r("\n    => ");
            $res = curl(
                "POST",
                "https://mall.shopee.co.id/api/v4/checkout/get",
                [
                    'content-type'      => 'application/json',
                    'accept'            => 'application/json',
                    'x-csrftoken'       => $settings["csrf"],
                    'x-sap-access-f'    => '3.2.108.4.0|13|2.5.0-2_5.4.223_0_2374|24e9df3b22a54764b2162f5d4e66ec669f1f587073ab42|10900|100',
                    'x-sap-access-s'    => '_Qsy31V-rBQUrJ8AeQZcWla4L-kDTzQhVyNVM5e1Joc=',
                    'x-sap-access-t'    => '1672034344',
                ],
                [],
                '
                {
                    "shoporders":[{
                        "shop":{
                            "shopid":' . $shopId . '
                        },
                        "items":[{
                                "itemid":' . $data["products"][$shopId]["item_id"] . ',
                                "modelid":' . $data["products"][$shopId]["model_id"] . ',
                                "quantity":1,
                                "item_group_id":"' . ($data["products"][$shopId]["item_group_id"] ?? 'null') . '"
                        }],
                        "shipping_id":1
                    }],
                    "selected_payment_channel_data":{
                        "channel_id":' . $data["payment"]["channel"]["id"] . ',
                        "channel_item_option_info":{
                            "option_info":"' . $data["payment"]["channel"]["option_info"] . '"
                        },
                        "version":2
                    },
                    "shipping_orders":[{
                        "sync":true,
                        "buyer_address_data":{
                            "addressid":' . $data["shipping"]["address_id"] . '
                        },
                        "selected_logistic_channelid":' . $shippings[$shopId]["channel"]["channelid"] . ',
                        "shipping_id":1,
                        "shoporder_indexes":[0],
                        "selected_preferred_delivery_time_option_id":' . $data["products"][$shopId]["logistic"]["delivery_time_option_id"] . '
                    }]
                }
                ',
                "ARRAY",
                "JSON"
            );

            if (@$res["result"]["checkout_price_data"] && $res["result"]['checkout_price_data']['merchandise_subtotal'] == $cartItem["price"]) {
                $checkout       = $res["result"];
                $totalPayment   = $res["result"]["checkout_price_data"]["total_payable"];

                print_r(date('Y-m-d H:i:s.') .  sprintf("%04d", @explode(".", microtime(true))[1]) . ' - Checkout is ' . ($checkout['can_checkout'] ? 'true' : 'false') . ' - ' . handleRupiahFormat($checkout['checkout_price_data']['total_payable']));

                $isWhile    = false;
            } else {
                json($res["status"], $res["result"]);
                $isWhile    = true;
            }
        } while ($isWhile);

        do {
            print_r("\n");
            print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - MAKE AN ORDER");
            print_r("\n    => Total Payment: " . handleRupiahFormat($totalPayment));
            print_r("\n    => ");
            $res = curl(
                "POST",
                "https://mall.shopee.co.id/api/v4/checkout/place_order",
                [
                    'content-type'  => 'application/json',
                    'accept'        => 'application/json',
                    'x-csrftoken'   => $settings["csrf"],
                ],
                [],
                '{
                    "client_id": 0,
                    "cart_type": 0,
                    "timestamp": ' . $checkout['timestamp'] . ',
                    "checkout_price_data": {
                        "merchandise_subtotal": ' . $checkout['checkout_price_data']['merchandise_subtotal'] . ',
                        "shipping_subtotal_before_discount": ' . $checkout['checkout_price_data']['shipping_subtotal_before_discount'] . ',
                        "shipping_discount_subtotal": ' . $checkout['checkout_price_data']['shipping_discount_subtotal'] . ',
                        "shipping_subtotal": ' . $checkout['checkout_price_data']['shipping_subtotal'] . ',
                        "tax_payable": ' . $checkout['checkout_price_data']['tax_payable'] . ',
                        "tax_exemption": ' . $checkout['checkout_price_data']['tax_exemption'] . ',
                        "custom_tax_subtotal": ' . $checkout['checkout_price_data']['custom_tax_subtotal'] . ',
                        "promocode_applied": null,
                        "credit_card_promotion": null,
                        "shopee_coins_redeemed": null,
                        "group_buy_discount": ' . $checkout['checkout_price_data']['group_buy_discount'] . ',
                        "bundle_deals_discount": null,
                        "price_adjustment": null,
                        "buyer_txn_fee": ' . $checkout['checkout_price_data']['buyer_txn_fee'] . ',
                        "buyer_service_fee": ' . $checkout['checkout_price_data']['buyer_service_fee'] . ',
                        "insurance_subtotal": ' . $checkout['checkout_price_data']['insurance_subtotal'] . ',
                        "insurance_before_discount_subtotal": ' . $checkout['checkout_price_data']['insurance_before_discount_subtotal'] . ',
                        "insurance_discount_subtotal": ' . $checkout['checkout_price_data']['insurance_discount_subtotal'] . ',
                        "vat_subtotal": ' . $checkout['checkout_price_data']['vat_subtotal'] . ',
                        "total_payable": ' . $checkout['checkout_price_data']['total_payable'] . '
                    },
                    "order_update_info": {},
                    "dropshipping_info": {
                        "enabled": false,
                        "name": "",
                        "phone_number": ""
                    },
                    "promotion_data": {
                        "can_use_coins": true,
                        "use_coins": false,
                        "platform_vouchers": [],
                        "free_shipping_voucher_info": {
                            "free_shipping_voucher_id": 0,
                            "free_shipping_voucher_code": "",
                            "disabled_reason": null,
                            "banner_info": {
                                "msg": "",
                                "learn_more_msg": ""
                            },
                            "required_be_channel_ids": [],
                            "required_spm_channels": []
                        },
                        "highlighted_platform_voucher_type": -1,
                        "shop_voucher_entrances": [
                            {
                                "shopid": ' . $shopId . ',
                                "status": true
                            }
                        ],
                        "applied_voucher_code": null,
                        "voucher_code": null,
                        "voucher_info": {
                            "coin_earned": 0,
                            "voucher_code": null,
                            "coin_percentage": 0,
                            "discount_percentage": 0,
                            "discount_value": 0,
                            "promotionid": 0,
                            "reward_type": 0,
                            "used_price": 0
                        },
                        "invalid_message": "",
                        "price_discount": 0,
                        "card_promotion_id": null,
                        "card_promotion_enabled": false,
                        "promotion_msg": ""
                    },
                    "selected_payment_channel_data": {
                        "version": 2,
                        "option_info": ' . json_encode($checkout['selected_payment_channel_data']['option_info']) . ',
                        "channel_id": ' . $checkout['selected_payment_channel_data']['channel_id'] . ',
                        "text_info": {}
                    },
                    "shoporders": [{
                            "shop": {
                                "shopid": ' . $shopId . '
                            },
                            "items": [{
                                "itemid": ' . $data["products"][$shopId]["item_id"] . ',
                                "modelid": ' . $data["products"][$shopId]["model_id"] . ',
                                "quantity": 1,
                                "item_group_id": "' . $checkout['shoporders'][0]['items'][0]['item_group_id'] . '",
                                "insurances": [],
                                "shopid": ' . $shopId . ',
                                "shippable": true,
                                "non_shippable_err": "",
                                "none_shippable_reason": "",
                                "none_shippable_full_reason": "",
                                "price": ' . $checkout['checkout_price_data']['merchandise_subtotal'] . ',
                                "is_add_on_sub_item": false,
                                "is_pre_order": false,
                                "is_streaming_price": false,
                                "checkout": true,
                                "is_spl_zero_interest": false,
                                "is_prescription": false,
                                "channel_exclusive_info": {
                                    "source_id": 0,
                                    "token": ""
                                },
                                "offerid": 0,
                                "supports_free_returns": false
                            }],
                            "tax_info": {
                                "use_new_custom_tax_msg": false,
                                "custom_tax_msg": "",
                                "custom_tax_msg_short": "",
                                "remove_custom_tax_hint": false
                            },
                            "tax_payable": ' . $checkout['checkout_price_data']['tax_payable'] . ',
                            "shipping_id": 1,
                            "shipping_fee_discount": ' . $checkout['checkout_price_data']['shipping_discount_subtotal'] . ',
                            "shipping_fee": ' . $checkout['checkout_price_data']['shipping_subtotal'] . ',
                            "order_total_without_shipping": ' . $checkout['checkout_price_data']['merchandise_subtotal'] . ',
                            "order_total": ' . ($checkout['checkout_price_data']['total_payable'] - $checkout['checkout_price_data']['buyer_service_fee']) . ',
                            "buyer_remark": "",
                            "ext_ad_info_mappings": []
                    }],
                    "shipping_orders": [
                        {
                            "shipping_id": 1,
                            "shoporder_indexes": [0],
                            "selected_logistic_channelid": ' . $shippings[$shopId]["channel"]["channelid"] . ',
                            "selected_preferred_delivery_time_option_id": ' . $data["products"][$shopId]["logistic"]["delivery_time_option_id"] . ',
                            "buyer_remark": "",
                            "buyer_address_data": {
                                "addressid": ' . $data["shipping"]["address_id"] . ',
                                "address_type": 0,
                                "tax_address": ""
                            },
                            "fulfillment_info": {
                                "fulfillment_flag": 64,
                                "fulfillment_source": "",
                                "managed_by_sbs": false,
                                "order_fulfillment_type": 2,
                                "warehouse_address_id": 0,
                                "is_from_overseas": false
                            },
                            "order_total": ' . ($checkout['checkout_price_data']['total_payable'] - $checkout['checkout_price_data']['buyer_service_fee']) . ',
                            "order_total_without_shipping": ' . $checkout['checkout_price_data']['merchandise_subtotal'] . ',
                            "selected_logistic_channelid_with_warning": null,
                            "shipping_fee": ' . $checkout['checkout_price_data']['shipping_subtotal'] . ',
                            "shipping_fee_discount": ' . $checkout['checkout_price_data']['shipping_discount_subtotal'] . ',
                            "shipping_group_description": "",
                            "shipping_group_icon": "",
                            "tax_payable": ' . $checkout['checkout_price_data']['tax_payable'] . ',
                            "is_fsv_applied": false,
                            "prescription_info": {
                                "images": null,
                                "required": false,
                                "max_allowed_images": 5
                            }
                        }
                    ],
                    "fsv_selection_infos": [],
                    "buyer_info": {
                        "share_to_friends_info": {
                            "display_toggle": false,
                            "enable_toggle": false,
                            "allow_to_share": false
                        },
                        "kyc_info": null,
                        "checkout_email": ""
                    },
                    "client_event_info": {
                        "is_platform_voucher_changed": false,
                        "is_fsv_changed": false
                    },
                    "buyer_txn_fee_info": {
                        "title": "Biaya Penanganan",
                        "description": "Biaya Penanganan untuk transaksi ini adalah Rp0",
                        "learn_more_url": ""
                    },
                    "disabled_checkout_info": {
                        "description": "",
                        "auto_popup": false,
                        "error_infos": []
                    },
                    "can_checkout": true,
                    "buyer_service_fee_info": {
                        "learn_more_url": "https://shopee.co.id/m/biaya-layanan"
                    },
                    "__raw": {},
                    "_cft": [3333739],
                    "captcha_version": 1,
                    "device_info": {
                        "device_sz_fingerprint": "Unw6657nP0Ycdw7Nw8G8jw==|f86Uqw/4uPyY3yyxB4iYxdQHu41r4N3DrsQTCzWa0XBA9ML0IPvXou0dlofrwKlvl+vO+ZSRgy6F3kOgqGg/yeAiGP/gdTGoNDMz|ZdTvMjKnTZ+JSHGy|06|3"
                    }
                }',
                "ARRAY",
                "JSON"
            );
            if ($res["status"] === true && @array_key_exists("checkoutid", $res["result"])) {
                json(true, $res["result"]);

                print_r("\n");
                print_r("\n" . date('Y-m-d H:i:s.') . sprintf("%04d", @explode(".", microtime(true))[1]) . " - " . sprintf("%07d", $i) . " - " . sprintf("%07d", $j) . " - END");
                print_r("\n    => SUCCESS");

                $j = 1337;
            } else {
                if (@array_key_exists("error", $res["result"])) {
                    json(false, @$res["result"]["error_msg"]);
                } else {
                    json(false, "Something error");
                }

                print_r("\n    => ");
                print_r(date('Y-m-d H:i:s.') .  sprintf("%04d", @explode(".", microtime(true))[1]));

                ++$j;
            }
        } while ($j <= 5);
    } else {
        ++$j;
    }
} while ($j <= 5);
exit;
